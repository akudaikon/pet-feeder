# Petnet SmartFeeder Gen 2 Replacement Firmware

This project uses the existing hardware, but removes the two existing on-board microcontrollers (Rigado BMD-300 and Silex SX-ULPGN) and replaces them with an ESP32.

**THIS IS A WORK IN PROGRESS. THERE MAY BE BUGS.**

### What it does:

- No longer dependent on the cloud/Petnet's servers. Completely local.
- Provides control via HTTP and MQTT.
- Allows you to schedule feedings of a set portion size at certain times, or just keep the bowl always topped off to the set portion.
- Keeps track of amount of food in food bin and food bowl.
- Keeps statistics of how much food was given and eaten each day.
- Provide means for low food or jams/dispensing issues alerts via MQTT flags, in conjunction with something like Home Assistant.

### What it doesn't do:

- Full backup battery support. The battery backup and charging still work, but it doesn't keep track of battery percentage or charging status. You won't get low battery alerts.
- Take any battery saving measures into account. The original firmware could turn things on and off to save battery life when on battery power (I think). This doesn't do any of that. That being said, it should still last around 8 hours on a fully charged battery.
- Provide fancy nutritional facts like the Petnet app does. This is fairly barebones, but functional.
- Maybe other things? I've never used the Petnet app or the SmartFeeder with Petnet's firmware, so I'm not aware of all of its original functionality.

## SmartFeeder Hardware
| Function | Part | Notes |
| --- | --- | --- |
| Main Processor | Rigado BMD-300 | NRF52 based, does all the heavy lifting |
| Secondary Processor | Silex SX-ULPGN | Provides WiFi, not sure what else |
| Load Cell ADC | Nuvoton NAU7802 | |
| LED Controller | ISSI IS31FL3193 | |
| Motor Driver | TI DRV8837 | |
| SPI Flash | IS25LQ040 | 4Mb, WP# pin pulled high in HW |
| Battery Charge Controller | Microchip MCP73832 | |

*See docs folder for more information*

## ESP32 to Petnet PCB Connection

The two microcontrollers on the Petnet PCB (Rigado BMD-300 and Silex SX-ULPGN) need to be removed or disabled somehow.

There are also two transistors on the top-side of the PCB that need to be jumped so that they remain always active (one under the brown 3-pin connector and one under the large 10-pin connector).

All of the ESP32 connection points below are on the bottom-side of the Petnet PCB.

| Petnet PCB | Function | ESP32 |
| --- | --- | --- |
| Rectangle Pad | GND | GND |
| TP152 | I2C SCL | GPIO13 |
| TP151 | I2C SDA | GPIO12 |
| TP308 | 5V | 5V **(make sure your ESP32 has a 3.3V regulator!)** |
| TP209 | DRV8837 IN1 | GPIO14 |
| TP206 | DRV8837 IN2 | GPIO27 |
| TP210 | DRV8837 nSleep | GPIO26 |
| TP316 | Front Button | GPIO25 |
| TP323 | Bin IR Sensor RX Output | GPIO33 |
| TP314 | Rotation Sensor Output | GPIO32 |

*The Petnet PCB has many other functions, but they're not used in this project for the sake of simplicity/lack of usefulness.*

## Initial Configuration

1. After turning on the SmartFeeder for the first time, the LED should be pulsing yellow. This means that you need to complete the WiFi setup. Using some device, connect to the WiFi network named "Pet Feeder". The WiFi configuration should automatically come up, or you'll be asked to log in to the "Pet Feeder" network which will take you to the WiFi configuration.
2. After completing the WiFi configuration, the SmartFeeder will attempt to connect to WiFi. The LED will pulse blue while it's attempting to connect. If the LED turns green, the WiFi connection was successful. If it turns red, it failed to connect and you'll need to repeat step 1.
3. After successfully connecting the WiFi, the SmartFeeder will restart and should begin pulsing purple. This means that you need to complete the weight sensor calibration. Using some device, open a browser and go to http://petfeeder.local or the IP address of the SmartFeeder. This should take you to the calibration page. Complete each of the calibration steps, then click the Continue button.
4. After calibration is completed, the SmartFeeder is ready to go. Going to http://petfeeder.local or the IP address of the SmartFeeder in a browser will take you to the web interface where you can view the status or change settings. You can also access the calibration process again if you need to recalibrate in the future.

## LED Meanings
| Color | Pattern | Meaning |
| --- | --- | --- |
| Red | Pulsing (quickly) | Factory reset in progress |
| Yellow | Pulsing (slowly) | Waiting for WiFi setup |
| Purple | Pulsing (slowly) | Waiting for calibration |
| Blue | Pulsing (slowly) | Connecting to WiFi |
| Red | Pulsing (slowly) | Food bin missing or food bin is empty |
| White | Pulsing (slowly) | Food dispensing in progress |
| Green | Solid | Amount of food in food bin is > 50% |
| Yellow | Solid | Amount of food in food bin is between 25% to 50% |
| Red | Solid | Amount of food in food bin is < 25% |

## Usage Tips

- Pressing and holding the front button for a couple seconds will manually dispense food up to the set portion amount. If the LED quickly pulses red for a couple seconds, the food bin is missing or is empty.
- You can also manually dispense food up to the set portion amount from the web interface.
- LED automatically dims from 10pm to 8am.
- If you ever need to factory reset the SmartFeeder, hold down the front button while turning it on. The LED should pulse red quickly for a few seconds. After that, the SmartFeeder will be reset and waiting for intial configuration again.

## HTTP Information

You can access the web interface by going to http://petfeeder.local or the IP address of the SmartFeeder in a browser.

```/action?feednow```
Dispense food up to the set portion size.

```/action?getstats```
Returns the current status and statistics in JSON format (in the same format that is published to the MQTT state topic described below).

```/action?resetstats```
Resets the day's statistics.

```/action?restart```
Reboot the SmartFeeder.

## MQTT Information

MQTT messages have been tailored for use with Home Assistant.

#### Availability Topic: ```petfeeder/availability```
A string that will either be ```online``` or ```offline``` depending on if the SmartFeeder is connected to MQTT.

#### State Topic: ```petfeeder/state```

```
{
  "binPercent": [int, percentage of food in food bin],
  "bowlAmount": [float, amount of food in bowl, in cups],
  "foodGiven":  [float, amount of food given today, in cups],
  "foodEaten":  [float, amount of food eaten today, in cups],
  "binLow":     [bool, flag to signify when food in bin is running low],
  "binEmpty":   [bool, flag to signify when food in bin is empty],
  "feedError":  [bool, flag to signify when there was an error dispensing food]
}
```

#### Home Assistant "Bowl Amount" Sensor Config Example:

```
sensor:
  - platform: "mqtt"
    name: petfeeder_bowlamount
    state_topic: "petfeeder/state"
    availability_topic: "petfeeder/availability"
    value_template: "{{ value_json.bowlAmount }}"
    unit_of_measurement: "cups"
```

#### Home Assistant "Food Bin Low" Notification Example:

```
sensor:
  - platform: "mqtt"
    name: petfeeder_binlow
    state_topic: "petfeeder/state"
    availability_topic: "petfeeder/availability"
    value_template: "{{ value_json.binLow }}"

automation:
  - alias: Pet Feeder Food Low
    trigger:
      platform: state
      entity_id: sensor.petfeeder_binlow
      to: 'True'
    action:
      service: notify.html5
      data:
        title: 'Pet Feeder'
        message: 'The food in the Pet Feeder is running low!'
```