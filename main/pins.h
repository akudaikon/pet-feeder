#ifndef PINS_H
#define PINS_H

#include "driver/gpio.h"

#define I2C_PORT_NUM            I2C_NUM_0
#define I2C_FREQ_HZ             100000

#define I2C_SDA_PIN             GPIO_NUM_12
#define I2C_SCL_PIN             GPIO_NUM_13

#define HX711_SCK_PIN           GPIO_NUM_23
#define HX711_DOUT_PIN          GPIO_NUM_22

#define BUTTON_PIN              GPIO_NUM_25

#define DRV8837_NSLEEP_PIN      GPIO_NUM_26
#define DRV8837_IN1_PIN         GPIO_NUM_14
#define DRV8837_IN2_PIN         GPIO_NUM_27

#define BIN_IR_PIN              GPIO_NUM_33
#define BIN_ROT_SENSOR_PIN      GPIO_NUM_32

#endif