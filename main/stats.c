#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include "cJSON.h"
#include "esp_err.h"
#include "esp_log.h"
#include "util.h"
#include "settings.h"
#include "stats.h"

#define STAT_WRITE_DELAY      30   // ~1 min

typedef struct statistics_t {
  uint8_t binPercent;
  uint8_t bowlAmount;
  uint8_t prevBowlAmount;
  uint8_t foodGiven;
  uint8_t foodEaten;
  bool feedError;
} statistics_t;

static statistics_t stats;
static bool flag_firstBowlAmount = true;

static uint8_t lastBowlAmount = 0;
static uint16_t bowlStableCount = 0;
static uint8_t lastBinPercent = 0;
static uint16_t binStableCount = 0;

esp_err_t stats_read()
{
  nvs_handle nvsHandle;
  esp_err_t err;

  time_t tnow = time(NULL);
  struct tm now = {};
  localtime_r(&tnow, &now);

  // Open stats NVS space
  err = nvs_open(STATS_NAMESPACE, NVS_READONLY, &nvsHandle);
  if (err != ESP_OK)
  {
    nvs_close(nvsHandle);

    // Write default stats
    err = nvs_open(STATS_NAMESPACE, NVS_READWRITE, &nvsHandle);
    if (err != ESP_OK)
    {
      stats.foodGiven = 0;
      stats.foodEaten = 0;

      nvs_set_u16(nvsHandle, "updateDay", now.tm_yday);
      nvs_set_u8(nvsHandle, "foodGiven", stats.foodGiven);
      nvs_set_u8(nvsHandle, "foodEaten", stats.foodEaten);

      nvs_commit(nvsHandle);
      nvs_close(nvsHandle);
    }

    nvs_open(STATS_NAMESPACE, NVS_READONLY, &nvsHandle);
  }

  // Read stats from NVS
  uint16_t temp;
  nvs_get_u16(nvsHandle, "updateDay", &temp);

  // If stored stat date is today, then use stored stats
  if (temp == now.tm_yday)
  {
    nvs_get_u8(nvsHandle, "foodGiven", &stats.foodGiven);
    nvs_get_u8(nvsHandle, "foodEaten", &stats.foodEaten);
  }
  // Otherwise reset stats
  else
  {
    stats.foodGiven = 0;
    stats.foodEaten = 0;
  }

  // Finally, close NVS
  nvs_close(nvsHandle);

  return ESP_OK;
}

static esp_err_t stats_write()
{
  nvs_handle nvsHandle;
  esp_err_t err;
  uint8_t temp8;
  uint16_t temp16;
  bool change = false;

  // Open stats NVS space
  err = nvs_open(STATS_NAMESPACE, NVS_READWRITE, &nvsHandle);
  if (err != ESP_OK) return err;

  time_t tnow = time(NULL);
  struct tm now = {};
  localtime_r(&tnow, &now);

  // Write stats, if changed
  nvs_get_u16(nvsHandle, "updateDay", &temp16);
  if (temp16 != now.tm_yday) { nvs_set_u16(nvsHandle, "updateDay", now.tm_yday); change = true; }

  nvs_get_u8(nvsHandle, "foodGiven", &temp8);
  if (temp8 != stats.foodGiven) { nvs_set_u8(nvsHandle, "foodGiven", stats.foodGiven); change = true; }
  
  nvs_get_u8(nvsHandle, "foodEaten", &temp8);
  if (temp8 != stats.foodEaten) { nvs_set_u8(nvsHandle, "foodEaten", stats.foodEaten); change = true; }

  // Commit stats to NVS, if changed
  if (change)
  {
    err = nvs_commit(nvsHandle);
    if (err != ESP_OK) return err;
  }

  // Finally, close NVS
  nvs_close(nvsHandle);
  return ESP_OK;
}

static void stats_writeUpdate()
{
  // Update stats
  if (stats.prevBowlAmount > stats.bowlAmount) stats.foodEaten += (stats.prevBowlAmount - stats.bowlAmount);
  stats.prevBowlAmount = stats.bowlAmount;
  stats_write();
}

void stats_setBowlAmount(uint8_t amount)
{
  stats.bowlAmount = amount;

  if (flag_firstBowlAmount)
  {
    stats.prevBowlAmount = stats.bowlAmount;
    bowlStableCount = 0;
    flag_firstBowlAmount = false;
  }

  if (stats.bowlAmount == lastBowlAmount)
  {
    if (++bowlStableCount >= (__UINT16_MAX__ - 1)) bowlStableCount = (__UINT16_MAX__ - 1);
  }
  else bowlStableCount = 0;

  lastBowlAmount = stats.bowlAmount;
}

bool stats_isBowlAmountStable(uint16_t numStableReadings)
{
  return (bowlStableCount >= numStableReadings);
}

uint8_t stats_getBowlAmount()
{
  return stats.bowlAmount;
}

void stats_setBinPercent(uint8_t percent)
{
  stats.binPercent = percent;

  if (stats.binPercent == lastBinPercent)
  {
    if (++binStableCount > (__UINT16_MAX__ - 1)) binStableCount = (__UINT16_MAX__ - 1);
  }
  else binStableCount = 0;

  lastBinPercent = stats.binPercent;
}

bool stats_isBinPercentStable(uint16_t numStableReadings)
{
  return (binStableCount >= numStableReadings);
}

void stats_addFoodGiven(uint8_t initialAmount, uint8_t newAmount)
{
  stats.foodGiven += (newAmount - initialAmount);
}

void stats_setFeedErrorFlag()
{
  stats.feedError = true;
}

void stats_clearFeedErrorFlag()
{
  stats.feedError = false;
}

void stats_reset()
{
  stats.foodEaten = 0;
  stats.foodGiven = 0;
  flag_firstBowlAmount = true;
  stats_write();
}

char* stats_toJSON()
{
  cJSON *root = NULL;
  root = cJSON_CreateObject();

  cJSON_AddNumberToObject(root, "binPercent", stats.binPercent);
  cJSON_AddDecimalToObject(root, "bowlAmount", (stats.bowlAmount * 0.125), 3);
  // cJSON_AddDecimalToObject(root, "prevBowlAmount", (stats.prevBowlAmount * 0.125), 3);    // DEBUG
  // cJSON_AddNumberToObject(root, "isBowlAmountStable", bowlStableCount);                   // DEBUG
  // cJSON_AddNumberToObject(root, "isBinPercentStable", binStableCount);                    // DEBUG
  cJSON_AddDecimalToObject(root, "foodGiven", (stats.foodGiven * 0.125), 3);
  cJSON_AddDecimalToObject(root, "foodEaten", (stats.foodEaten * 0.125), 3);
  cJSON_AddBoolToObject(root, "binLow", (stats.binPercent < SETTINGS_BIN_LOW_PERCENT ? true : false));
  cJSON_AddBoolToObject(root, "binEmpty", (stats.binPercent < SETTINGS_BIN_EMPTY_PERCENT ? true : false));
  cJSON_AddBoolToObject(root, "feedError", stats.feedError);

  char *jsonStats = cJSON_PrintUnformatted(root);
  cJSON_Delete(root);

  return jsonStats;
}

void stats_update(bool forceUpdate)
{
  // write_timer is used to ensure we don't update statistics until steady state
  // Also provides rate limiting to flash writes
  if (!forceUpdate)
  {
    if ((stats.bowlAmount != stats.prevBowlAmount) && stats_isBowlAmountStable(STAT_WRITE_DELAY)) stats_writeUpdate();
  }
  else
  {
    // Force update, so write now!
    stats_writeUpdate();
  }

  ESP_LOGI(__func__, "Statistics - Food Given: %d, Food Eaten: %d", stats.foodGiven, stats.foodEaten); 
}

void stats_init()
{
  stats.binPercent = 0;
  stats.bowlAmount = 0;
  stats.prevBowlAmount = 0;
  stats.foodGiven = 0;
  stats.foodEaten = 0;
  stats.feedError = false;
}
