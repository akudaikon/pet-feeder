#ifndef MQTT_H
#define MQTT_H

esp_err_t mqtt_start(char* uri);
void mqtt_stop();
void mqtt_sendUpdate();

#endif