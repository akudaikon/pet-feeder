#include <string.h>
#include <fcntl.h>

#include "esp_http_server.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_spiffs.h"
#include "esp_vfs.h"
#include "esp_ota_ops.h"
#include "cJSON.h"

#include "eventgroup_defs.h"
#include "settings.h"
#include "stats.h"
#include "mqtt.h"
#include "util.h"
#include "IS31FL3193.h"

static httpd_handle_t server = NULL;

#define FILE_PATH_MAX (ESP_VFS_PATH_MAX + CONFIG_SPIFFS_OBJ_NAME_LEN)
#define IS_FILE_EXT(filename, ext) (strcasecmp(&filename[strlen(filename) - sizeof(ext) + 1], ext) == 0)

static esp_err_t set_content_type_from_file(httpd_req_t *req, const char *filename)
{
  if (IS_FILE_EXT(filename, ".htm")) return httpd_resp_set_type(req, "text/html");
  else if (IS_FILE_EXT(filename, ".css")) return httpd_resp_set_type(req, "text/css");
  else if (IS_FILE_EXT(filename, ".js")) return httpd_resp_set_type(req, "text/javascript");
  return httpd_resp_set_type(req, "text/plain");
}

static esp_err_t wifiScan(httpd_req_t *req)
{
  wifi_scan_config_t wifi_scan_config =
  {
    .ssid = 0,
    .bssid = 0,
    .channel = 0,
    .show_hidden = false,
    .scan_type = WIFI_SCAN_TYPE_ACTIVE,
    .scan_time.active.min = 100,
    .scan_time.active.max = 300,
  };
  esp_wifi_scan_start(&wifi_scan_config, true);

  uint16_t scanCount = 0;
  esp_wifi_scan_get_ap_num(&scanCount);

  wifi_ap_record_t scanResult[scanCount];
  esp_wifi_scan_get_ap_records(&scanCount, scanResult);

  // Remove duplicate SSIDs (keeps the strongest RSSI)
  for (uint16_t res = 0; res < scanCount; res++)
  {
    if (scanResult[res].ssid[0] == '\0') continue;

    char cssid[33] = { 0 };
    strncpy(cssid, (char*)scanResult[res].ssid, 33);

    for (uint16_t cres = res + 1; cres < scanCount; cres++)
    {
      if (!strcmp(cssid, (char*)scanResult[cres].ssid)) scanResult[cres].ssid[0] = '\0';
    }
  }

  httpd_resp_set_status(req, HTTPD_200);
  httpd_resp_set_hdr(req, "Content-Type", "application/json");

  httpd_resp_sendstr_chunk(req, "[");
  for (uint16_t res = 0; res < scanCount; res++)
  {
    if (scanResult[res].ssid[0] == '\0') continue;  // dup

    httpd_resp_sendstr_chunk(req, "{\"ssid\": \"");
    httpd_resp_sendstr_chunk(req, (char*)scanResult[res].ssid);
    httpd_resp_sendstr_chunk(req, "\", \"rssi\": ");

    char rssiChar[5];
    itoa(scanResult[res].rssi, rssiChar, 10);
    httpd_resp_sendstr_chunk(req, rssiChar);

    if (scanResult[res].authmode > 0) httpd_resp_sendstr_chunk(req, ", \"auth\": true}");
    else httpd_resp_sendstr_chunk(req, ", \"auth\": false}");

    httpd_resp_sendstr_chunk(req, ",");
  }
  httpd_resp_sendstr_chunk(req, "{}]"); // extra item is a hack to always create valid JSON due to always trailing ','

  httpd_resp_sendstr_chunk(req, NULL);
  return ESP_OK;
}

static esp_err_t saveSettings(httpd_req_t *req)
{
  if (req->content_len > 1023)
  {
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Content too long");
    return ESP_FAIL;
  }

  uint16_t cur_len = 0;
  char respData[1024] = { 0 };
  while (cur_len < req->content_len)
  {
    uint16_t received = httpd_req_recv(req, respData + cur_len, 1023);
    if (received <= 0)
    {
      httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Error reading response data");
      return ESP_FAIL;
    }
    cur_len += received;
  }
  respData[req->content_len] = '\0';

  ESP_LOGI(__func__, "%s", respData);

  uint8_t retStatus = settings_fromJSON(respData);
  settings_write();

  if (retStatus == SETTINGS_NEEDRESTART)
  {
    httpd_resp_set_status(req, "202 Accepted");
    httpd_resp_send(req, "Settings saved. Restarting...", 29);
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    esp_restart();
  }
  else if (retStatus == SETTINGS_OK)
  {
    httpd_resp_set_status(req, HTTPD_200);
    httpd_resp_send(req, "Settings saved.", 15);
    return ESP_OK;
  }

  httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, NULL);
  return ESP_FAIL;
}

static esp_err_t updateSPIFFS(httpd_req_t *req)
{
  const esp_partition_t *update_partition = NULL;

  ESP_LOGI(__func__, "Starting SPIFFS OTA...");

  update_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_SPIFFS, NULL);
  if (update_partition == NULL)
  {
    ESP_LOGE(__func__, "SPIFFS partition not found");
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "SPIFFS partition not found");
    return ESP_FAIL;
  }
  ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);

  settings.firmwareUpdating = true;

  // Set LED to breathe white quickly
  led_enableBreathing(LED_OFF_TIME_130MS, LED_ON_TIME_130MS, LED_FADE_TIME_130MS);
  led_setColor(0xFF, 0xFF, 0xFF);

  ESP_LOGI(__func__, "Erasing SPIFFS partition...");
  esp_err_t err = esp_partition_erase_range(update_partition, 0, update_partition->size);
  if (err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error erasing SPIFFS partition, error=%d", err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Error erasing SPIFFS partition");
    goto spiffs_fail;
  }
  ESP_LOGI(__func__, "SPIFFS partition erase successful");

  // Start writing SPIFFS OTA data to partition
  ESP_LOGI(__func__, "Writing data to SPIFFS partition...");
  esp_err_t write_err = ESP_OK;
  size_t data_offset = 0;
  while (1)
  {
    char chunk[1024] = { 0 };
    int recv_len = httpd_req_recv(req, chunk, 1024);
    if (recv_len < 0) { write_err = ESP_ERR_INVALID_RESPONSE; break; }
    if (recv_len == 0) break;

    write_err = esp_partition_write(update_partition, data_offset, (const void *)chunk, recv_len);
    if (write_err != ESP_OK) break;
    data_offset += recv_len;
  }

  if (write_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_partition_write failed! err=0x%d", write_err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "esp_partition_write failed!");
    goto spiffs_fail;
  }

  ESP_LOGI(__func__, "SPIFFS OTA successful!");
  httpd_resp_set_status(req, HTTPD_200);
  httpd_resp_send(req, "SPIFFS update successful!", 25);

  // Set LED to solid green
  led_disableBreathing();
  led_setColor(0x22, 0xFF, 0x00);
  goto spiffs_success;

  spiffs_fail:
    // Set LED to solid red
    led_disableBreathing();
    led_setColor(0xFF, 0x00, 0x00);

  spiffs_success:
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    esp_restart();
}

static esp_err_t updateFirmware(httpd_req_t *req)
{
  esp_ota_handle_t update_handle = 0;
  const esp_partition_t *update_partition = NULL;

  ESP_LOGI(__func__, "Starting Firmware OTA...");

  update_partition = esp_ota_get_next_update_partition(NULL);
  if (update_partition == NULL)
  {
    ESP_LOGE(__func__, "Firmware OTA partition not found");
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Firmware OTA partition not found");
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);

  esp_err_t err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
  if (err != ESP_OK)
  {
    ESP_LOGE(__func__, "esp_ota_begin failed, error=%d", err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "esp_ota_begin failed");
    return ESP_FAIL;
  }

  ESP_LOGI(__func__, "esp_ota_begin succeeded");

  settings.firmwareUpdating = true;

  // Set LED to breathe white quickly
  led_enableBreathing(LED_OFF_TIME_130MS, LED_ON_TIME_130MS, LED_FADE_TIME_130MS);
  led_setColor(0xFF, 0xFF, 0xFF);

  esp_err_t ota_write_err = ESP_OK;
  while (1)
  {
    char chunk[1024] = { 0 };
    int recv_len = httpd_req_recv(req, chunk, 1023);
    if (recv_len < 0) { ota_write_err = ESP_FAIL; break; }
    if (recv_len == 0) break;

    ota_write_err = esp_ota_write(update_handle, (const void *)chunk, recv_len);
    if (ota_write_err != ESP_OK) break;
  }

  esp_err_t ota_end_err = esp_ota_end(update_handle);
  if (ota_write_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_ota_write failed! err=0x%d", err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "esp_ota_write failed!");
    goto firmware_fail;
  }
  else if (ota_end_err != ESP_OK)
  {
    ESP_LOGE(__func__, "Error: esp_ota_end failed! err=0x%d. Image is invalid", ota_end_err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "esp_ota_end failed! Image is invalid");
    goto firmware_fail;
  }

  if (esp_ota_set_boot_partition(update_partition) != ESP_OK)
  {
    ESP_LOGE(__func__, "esp_ota_set_boot_partition failed! err=0x%d", err);
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "esp_ota_set_boot_partition failed!");
    goto firmware_fail;
  }

  ESP_LOGI(__func__, "Firmware OTA successful!");
  httpd_resp_set_status(req, HTTPD_200);
  httpd_resp_send(req, "Update successful! Restarting...", 32);

  // Set LED to solid green
  led_disableBreathing();
  led_setColor(0x22, 0xFF, 0x00);
  goto firmware_success;

  firmware_fail:
    // Set LED to solid red
    led_disableBreathing();
    led_setColor(0xFF, 0x00, 0x00);

  firmware_success:
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    esp_restart();
}

static esp_err_t default_get_handler(httpd_req_t *req)
{
  if (!settings.configured)
  {
    // Handle redirect to captive portal if client tries to access another host
    size_t hostHdrLen = httpd_req_get_hdr_value_len(req, "Host") + 1;
    if (hostHdrLen > 1)
    {
      char* hostHdr = malloc(hostHdrLen);
      if (httpd_req_get_hdr_value_str(req, "Host", hostHdr, hostHdrLen) == ESP_OK)
      {
        if (strcmp(hostHdr, "192.168.4.1"))
        {
          httpd_resp_set_status(req, "302 Found");
          httpd_resp_set_hdr(req, "Location", "http://192.168.4.1");
          httpd_resp_send(req, NULL, 0);
          free(hostHdr);
          return ESP_OK;
        }
      }
      free(hostHdr);
    }
  }

  char filepath[FILE_PATH_MAX];
  struct stat file_stat;

  strncpy(filepath, req->uri, sizeof(filepath));

  // If root is requested, redirect based on situation
  if (!strcmp(filepath, "/"))
  {
    if (!settings.configured) strlcat(filepath, "wifi.htm", sizeof(filepath));
    else if (settings.calibrated != SETTINGS_CALIBRATION_DONE) strlcat(filepath, "calib.htm", sizeof(filepath));
    else strlcat(filepath, "index.htm", sizeof(filepath));
  }

  ESP_LOGI(__func__, "Sending file: %s...", filepath);

  // If file doesn't exist
  if (stat(filepath, &file_stat) == -1)
  {
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "File does not exist");
    return ESP_FAIL;
  }

  FILE *fd = fopen(filepath, "rb");
  if (fd == NULL)
  {
    httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to read existing file");
    return ESP_FAIL;
  }

  set_content_type_from_file(req, filepath);

  // Keep looping until whole file is sent
  while(1)
  {
    // Read file in chunks into buffer
    char chunk[512] = { 0 };
    size_t chunksize = fread(chunk, sizeof(char), 512, fd);
    if (chunksize == 0) break;  // EOF
    
    // Send the buffer contents as HTTP response chunk
    if (httpd_resp_send_chunk(req, chunk, chunksize) != ESP_OK)
    {
      fclose(fd);
      httpd_resp_sendstr_chunk(req, NULL);
      httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to send file");
      return ESP_FAIL;
    }
  }

  // Close file after sending complete
  fclose(fd);

  // Respond with an empty chunk to signal HTTP response completion
  httpd_resp_send_chunk(req, NULL, 0);
  return ESP_OK;
}

static esp_err_t action_get_handler(httpd_req_t *req)
{
  char* query;
  size_t queryLen = httpd_req_get_url_query_len(req) + 1;

  if (queryLen > 1)
  {
    query = malloc(queryLen);
    if (httpd_req_get_url_query_str(req, query, queryLen) == ESP_OK)
    {
      ESP_LOGI(__func__, "%s", query);

      if (!strcmp(query, "wifiscan")) return wifiScan(req);
      else if (!strcmp(query, "getsettings"))
      {
        char *jsonSettings = settings_toJSON();
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_set_hdr(req, "Content-Type", "application/json");
        httpd_resp_send(req, jsonSettings, strlen(jsonSettings));
        free(jsonSettings);
        return ESP_OK;
      }
      else if (!strcmp(query, "getcalib"))
      {
        char *jsonSettings = settings_calibDataToJSON();
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_set_hdr(req, "Content-Type", "application/json");
        httpd_resp_send(req, jsonSettings, strlen(jsonSettings));
        free(jsonSettings);
        return ESP_OK;
      }
      else if (!strcmp(query, "getstats"))
      {
        char *jsonStats = stats_toJSON();
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_set_hdr(req, "Content-Type", "application/json");
        httpd_resp_send(req, jsonStats, strlen(jsonStats));
        free(jsonStats);
        return ESP_OK;
      }
      else if (!strcmp(query, "resetstats"))
      {
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_send(req, "Statistics have been reset!", 27);
        xEventGroupSetBits(xEventGroup, END_OF_DAY_AFTER);
        return ESP_OK;
      }
      else if (!strcmp(query, "feednow"))
      {
        if (stats_getBowlAmount() < settings.feedAmount)
        {
          httpd_resp_set_status(req, HTTPD_200);
          httpd_resp_send(req, "Feeding now...", 14);
          xEventGroupSetBits(xEventGroup, BUTTON_LONG_PRESS);
        }
        else
        {
          httpd_resp_set_status(req, "202 Accepted");
          httpd_resp_send(req, "Bowl already full!", 18);
        }
        return ESP_OK;
      }
      else if (!strcmp(query, "restart"))
      {
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_send(req, "Restarting...", 13);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        esp_restart();
      }
      else if (!strcmp(query, "resetsettings"))
      {
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_send(req, "Reseting to factory defaults and restarting...", 46);
        settings_clear();
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        esp_restart();
      }
      else if (!strcmp(query, "resetcalib"))
      {
        httpd_resp_set_status(req, HTTPD_200);
        httpd_resp_send(req, "Reseting calibration and restarting...", 38);
        settings.calibrated = 0;
        settings_write();
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        esp_restart();
      }
      else
      {
        esp_err_t respErr = ESP_OK;
        char value[MAX_STRING_SIZE];
        if (httpd_query_key_value(query, "calib", value, sizeof(value)) == ESP_OK)
        {
          if (!strcasecmp(value, "binEmpty")) xEventGroupSetBits(xEventGroup, CALIB_BIN_EMPTY);
          else if (!strcasecmp(value, "binFull")) xEventGroupSetBits(xEventGroup, CALIB_BIN_FULL);
          else if (!strcasecmp(value, "bowlFood")) xEventGroupSetBits(xEventGroup, CALIB_BOWL_FOOD);
          else if (!strcasecmp(value, "waterEmpty")) xEventGroupSetBits(xEventGroup, CALIB_WATER_EMPTY);
          else if (!strcasecmp(value, "waterFull")) xEventGroupSetBits(xEventGroup, CALIB_WATER_FULL);
          else respErr = ESP_FAIL;
        }

        if (respErr == ESP_OK)
        {
          httpd_resp_send(req, NULL, 0);
          return ESP_OK;
        }
      }
    }
  }

  httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, NULL);
  return ESP_FAIL;
}

static esp_err_t action_post_handler(httpd_req_t *req)
{
  char* query;
  size_t queryLen = httpd_req_get_url_query_len(req) + 1;

  if (queryLen > 1)
  {
    query = malloc(queryLen);
    if (httpd_req_get_url_query_str(req, query, queryLen) == ESP_OK)
    {
      ESP_LOGI(__func__, "%s", query);
      if (!strcmp(query, "savesettings")) return saveSettings(req);
      else
      {
        char value[MAX_STRING_SIZE];
        if (httpd_query_key_value(query, "update", value, sizeof(value)) == ESP_OK)
        {
          if (!strcmp(value, "firmware")) return updateFirmware(req);
          else if (!strcmp(value, "spiffs")) return updateSPIFFS(req);
        }
      }
    }
  }

  httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, NULL);
  return ESP_FAIL;
}

static esp_err_t wificonfig_get_handler(httpd_req_t *req)
{
  char* query;
  size_t queryLen = httpd_req_get_url_query_len(req) + 1;

  if (queryLen > 1)
  {
    query = malloc(queryLen);
    if (httpd_req_get_url_query_str(req, query, queryLen) == ESP_OK)
    {
      ESP_LOGI(__func__, "%s", query);
      char value[MAX_STRING_SIZE];

      if (httpd_query_key_value(query, "wifiSSID", value, sizeof(value)) == ESP_OK)
      {
        char *temp = malloc(strlen(value));
        temp = urldecode(value, strlen(value));
        strncpy(settings.wifiSSID, temp, 32);
        free(temp);
      }
      if (httpd_query_key_value(query, "wifiPassword", value, sizeof(value)) == ESP_OK)
      {
        char *temp = malloc(strlen(value));
        temp = urldecode(value, strlen(value));
        strncpy(settings.wifiPassword, temp, 64);
        free(temp);
      }

      httpd_resp_set_status(req, HTTPD_200);
      httpd_resp_send(req, "Settings saved! Attempting to connect to Wifi...", 48);

      xEventGroupSetBits(xEventGroup, CONFIG_DONE);
      return ESP_OK;
    }
  }
  httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, NULL);
  return ESP_FAIL;
}

static esp_err_t update_get_handler(httpd_req_t *req)
{
  // update.htm is embedded in program code (in component.mk)
  extern const unsigned char update_htm_start[] asm("_binary_update_htm_start");
  extern const unsigned char update_htm_end[]   asm("_binary_update_htm_end");
  const size_t update_htm_size = (update_htm_end - update_htm_start);
  httpd_resp_set_type(req, "text/html");
  httpd_resp_send(req, (const char *)update_htm_start, update_htm_size);
  return ESP_OK;
}

void httpServer_start()
{
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.core_id = 1;
  config.uri_match_fn = httpd_uri_match_wildcard;
  config.lru_purge_enable = true;

  if (httpd_start(&server, &config) != ESP_OK)
  {
    ESP_LOGI(__func__, "HTTP server failed to start");
    return;
  }

  httpd_uri_t wificonfig_get_uri = {
    .uri = "/wificonfig",
    .method = HTTP_GET,
    .handler = wificonfig_get_handler,
    .user_ctx = NULL
  };
  httpd_register_uri_handler(server, &wificonfig_get_uri);

  httpd_uri_t update_get_uri = {
    .uri = "/update",
    .method = HTTP_GET,
    .handler = update_get_handler,
    .user_ctx = NULL
  };
  httpd_register_uri_handler(server, &update_get_uri);

  httpd_uri_t action_get_uri = {
    .uri = "/action",
    .method = HTTP_GET,
    .handler = action_get_handler,
    .user_ctx = NULL
  };
  httpd_register_uri_handler(server, &action_get_uri);

  httpd_uri_t action_post_uri = {
    .uri = "/action",
    .method = HTTP_POST,
    .handler = action_post_handler,
    .user_ctx = NULL
  };
  httpd_register_uri_handler(server, &action_post_uri);

  httpd_uri_t default_get_uri = {
    .uri = "/*",  // catch all
    .method = HTTP_GET,
    .handler = default_get_handler,
    .user_ctx = NULL
  };
  httpd_register_uri_handler(server, &default_get_uri);

  xEventGroupSetBits(xEventGroup, HTTP_SERVER_STARTED);
  ESP_LOGI(__func__, "HTTP server started");
}

void httpServer_stop()
{
  if (server != NULL)
  {
    httpd_stop(server);
    xEventGroupClearBits(xEventGroup, HTTP_SERVER_STARTED);
    ESP_LOGI(__func__, "HTTP server stopped");
  }
}
