#ifndef NAU7802_H
#define NAU7802_H

#define NAU7802_I2C_ADDR            0x2A

#define NAU7802_REG_PU_CTRL         0x00
#define NAU7802_REG_CTRL1           0x01
#define NAU7802_REG_CTRL2           0x02
#define NAU7802_REG_OCAL1_B2        0x03
#define NAU7802_REG_OCAL1_B1        0x04
#define NAU7802_REG_OCAL1_B0        0x05
#define NAU7802_REG_GCAL1_B3        0x06
#define NAU7802_REG_GCAL1_B2        0x07
#define NAU7802_REG_GCAL1_B1        0x08
#define NAU7802_REG_GCAL1_B0        0x09
#define NAU7802_REG_OCAL2_B2        0x0A
#define NAU7802_REG_OCAL2_B1        0x0B
#define NAU7802_REG_OCAL2_B0        0x0C
#define NAU7802_REG_GCAL2_B3        0x0D
#define NAU7802_REG_GCAL2_B2        0x0E
#define NAU7802_REG_GCAL2_B1        0x0F
#define NAU7802_REG_I2C_CTRL        0x11
#define NAU7802_REG_ADCO_B2         0x12
#define NAU7802_REG_ADCO_B1         0x13
#define NAU7802_REG_ADCO_B0         0x14
#define NAU7802_REG_OTP_B1          0x15
#define NAU7802_REG_OTP_B0          0x16
#define NAU7802_REG_PGA             0x1B
#define NAU7802_REG_DEV_REV         0x1F

#define NAU7802_CH_BOWL             0x00
#define NAU7802_CH_BIN              0x80

#define NAU7802_BIT_PUR             0x08
#define NAU7802_BIT_CR              0x20
#define NAU7802_BIT_CALS            0x04
#define NAU7802_BIT_CAL_ERR         0x08

#define NAU7802_MIN_CONVERSIONS     6

void NAU7802_init();
void NAU7802_calibrate(uint8_t channel);
void NAU7802_setChannel(uint8_t channel);
int32_t NAU7802_readValue();

#endif