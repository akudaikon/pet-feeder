#ifndef EVENTGROUP_DEFS_H
#define EVENTGROUP_DEFS_H

#include "freertos/event_groups.h"

#define SCHEDULER_TASK_STARTED   BIT0
#define BUTTON_TASK_STARTED      BIT1
#define HTTP_SERVER_STARTED      BIT2
#define MQTT_CLIENT_STARTED      BIT3

#define BUTTON_PRESS             BIT4
#define BUTTON_LONG_PRESS        BIT5
#define SCHEDULER_EVENT          BIT6
#define LED_DIM                  BIT7
#define END_OF_DAY_BEFORE        BIT8
#define END_OF_DAY_AFTER         BIT9
#define TIME_SET                 BIT10

#define CONFIG_MODE              BIT11
#define CONFIG_DONE              BIT12

#define WIFI_CONNECTED           BIT13
#define WIFI_DISCONNECTED        BIT14
#define MQTT_CONNECTED           BIT15

#define CALIB_BIN_EMPTY          BIT16
#define CALIB_BIN_FULL           BIT17
#define CALIB_BOWL_FOOD          BIT18
#define CALIB_WATER_EMPTY        BIT19
#define CALIB_WATER_FULL         BIT20

extern EventGroupHandle_t xEventGroup;

#endif
