#ifndef WEIGHT_H
#define WEIGHT_H

void weight_init();
void weight_recalibrate();

bool weight_measureBowl();
int32_t weight_calibrateBowl();

bool weight_measureBin();
int32_t weight_calibrateBin();

void weight_measureWater();
int32_t weight_calibrateWater();

uint8_t weight_getBowlAmount();
uint8_t weight_getBinPercentage();
uint8_t weight_getWaterPercentage();

void weight_resetAverages();

#endif