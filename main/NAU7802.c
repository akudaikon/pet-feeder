#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/i2c.h"

#include "pins.h"
#include "util.h"
#include "NAU7802.h"

static uint8_t currentChannel = 255;  // purposefully invalid initial value

static void NAU7802_write(uint8_t reg_addr, uint8_t data)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (NAU7802_I2C_ADDR << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, data, ACK_CHECK_EN);
  i2c_master_stop(cmd);
  i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
}

static bool NAU7802_checkBit(uint8_t reg_addr, uint8_t bit_mask)
{
  uint8_t value = 0;

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (NAU7802_I2C_ADDR << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (NAU7802_I2C_ADDR << 1) | I2C_MASTER_READ, ACK_CHECK_EN);
  i2c_master_read_byte(cmd, &value, NACK_VAL);
  i2c_master_stop(cmd);
  esp_err_t ret = i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);

  if (ret != ESP_OK) return false;
  if (value & bit_mask) return true;
  return false;
}

static void NAU7802_waitForBit(uint8_t reg_addr, uint8_t bit_mask, bool bit_set)
{
  bool value = false;

  do
  {
    vTaskDelay(1 / portTICK_PERIOD_MS);
    value = NAU7802_checkBit(reg_addr, bit_mask);
  } while (value != bit_set);
}

void NAU7802_calibrate(uint8_t channel)
{
  if (channel != NAU7802_CH_BIN && channel != NAU7802_CH_BOWL) return;

  NAU7802_write(NAU7802_REG_CTRL2, (channel | NAU7802_BIT_CALS));   // start cal for channel, set CALS
  NAU7802_waitForBit(NAU7802_REG_CTRL2, NAU7802_BIT_CALS, false);   // wait until CALS cleared
  currentChannel = 255;                                             // make sure we reset the channel in setChannel()
}

void NAU7802_init()
{
  ESP_LOGI(__func__, "NAU7802 init start");

  NAU7802_write(NAU7802_REG_PU_CTRL, 0x01);                         // set RR bit
  vTaskDelay(1 / portTICK_PERIOD_MS);
  NAU7802_write(NAU7802_REG_PU_CTRL, 0x02);                         // clear RR and set PUD
  NAU7802_waitForBit(NAU7802_REG_PU_CTRL, NAU7802_BIT_PUR, true);   // wait until PUR set
  NAU7802_write(NAU7802_REG_PU_CTRL, 0x8E);                         // set PUD, PUA, PUR, and AVDDS
  NAU7802_write(NAU7802_REG_OTP_B1, 0x30);                          // turn off ADC chopper
  //NAU7802_write(NAU7802_REG_PGA, 0x01);                             // turn off PGA chopper
  NAU7802_write(NAU7802_REG_CTRL1, 0x2C);                           // set VLDO=3.0, 16x gain

  NAU7802_calibrate(NAU7802_CH_BOWL);
  NAU7802_calibrate(NAU7802_CH_BIN);
  ESP_LOGI(__func__, "NAU7802 calibration done");

  NAU7802_write(NAU7802_REG_PU_CTRL, 0x9E);                         // set CS

  // wait for a bunch of conversions to make sure initial values are stable
  for (uint8_t x = 0; x < NAU7802_MIN_CONVERSIONS; x++) NAU7802_readValue();

  ESP_LOGI(__func__, "NAU7802 init done");
}

void NAU7802_setChannel(uint8_t channel)
{
  if (channel != NAU7802_CH_BIN && channel != NAU7802_CH_BOWL) return;

  // if the current channel isn't the one we're changing to, make sure we wait for enough conversions
  if (channel != currentChannel)
  {
    // set new channel
    NAU7802_write(NAU7802_REG_CTRL2, channel);
    currentChannel = channel;

    // because there's actually only one ADC for both channels, we need to wait for 
    // enough conversions before we get a stable, accurate value after a channel change
    for (uint8_t x = 0; x < NAU7802_MIN_CONVERSIONS; x++) NAU7802_readValue();
  }
}

int32_t NAU7802_readValue()
{
  // wait for CR bit set to signify conversion ready
  NAU7802_waitForBit(NAU7802_REG_PU_CTRL, NAU7802_BIT_CR, true);

  // read ADC register, 3 bytes
  uint8_t temp[3];
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (NAU7802_I2C_ADDR << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, NAU7802_REG_ADCO_B2, ACK_CHECK_EN);
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (NAU7802_I2C_ADDR << 1) | I2C_MASTER_READ, ACK_CHECK_EN);
  i2c_master_read_byte(cmd, &temp[0], ACK_VAL);
  i2c_master_read_byte(cmd, &temp[1], ACK_VAL);
  i2c_master_read_byte(cmd, &temp[2], NACK_VAL);
  i2c_master_stop(cmd);
  esp_err_t ret = i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);

  // If we got a NACK when trying to read conversion result, return -1 to signify invalid data
  if (ret != ESP_OK) return -1;

  // construct 24 bit conversion result and sign extend to 32 bits
  uint32_t value_raw = (uint32_t)temp[0] << 16;
  value_raw |= (uint32_t)temp[1] << 8;
  value_raw |= (uint32_t)temp[2];
  if (value_raw & 0x800000) value_raw |= 0xff000000;
  int32_t value_signed = (int32_t)value_raw;

  // only 18 "noise-free" bits at these settings, so shift down to 18 bits
  value_signed >>= 6;
  return value_signed;
}
