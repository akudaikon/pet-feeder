#ifndef SETTINGS_H
#define SETTINGS_H

#define SETTINGS_NAMESPACE  "settings"
#define MAX_STRING_SIZE     256

#define SETTINGS_CALIBRATION_BIN_EMPTY    BIT0
#define SETTINGS_CALIBRATION_BIN_FULL     BIT1
#define SETTINGS_CALIBRATION_BOWL_FOOD    BIT2
#define SETTINGS_CALIBRATION_WATER_EMPTY  BIT3
#define SETTINGS_CALIBRATION_WATER_FULL   BIT4
#define SETTINGS_CALIBRATION_DONE         (BIT0 | BIT1 | BIT2)

#define SETTINGS_BIN_WARNING_PERCENT      50
#define SETTINGS_BIN_LOW_PERCENT          25
#define SETTINGS_BIN_EMPTY_PERCENT        15

#define SETTINGS_FEEDMODE_SCHEDULE        true
#define SETTINGS_FEEDMODE_ALWAYSFULL      false

#define SETTINGS_OK                       0
#define SETTINGS_NEEDRESTART              1
#define SETTINGS_ERROR                    2

typedef struct {
  bool   enabled;
  int8_t week_day;  // -1 for every day of week
  int8_t hour;
  int8_t minute;
} scheduler_event_t;

typedef struct settingsData_t
{
  uint8_t configured;
  
  uint8_t calibrated;
  int32_t calibration_bin_empty;
  int32_t calibration_bin_full;
  int32_t calibration_bowl_empty;
  int32_t calibration_bowl_food;
  int32_t calibration_water_empty;
  int32_t calibration_water_full;

  uint8_t feedMode;
  uint8_t feedAmount;
  uint8_t enableMissedFeed;

  uint8_t timezone;
  scheduler_event_t schedulerEvents[4];

  char wifiSSID[32];
  char wifiPassword[64];

  uint8_t mqttEnable;
  char mqttURI[MAX_STRING_SIZE];

  char ipAddress[16];
  bool mqttConnected;
  bool firmwareUpdating;
} settingsData_t;

extern settingsData_t settings;

esp_err_t nvs_init();
esp_err_t settings_init();
esp_err_t settings_read();
esp_err_t settings_write();
uint8_t settings_fromJSON(char* json);
char* settings_toJSON();
char* settings_calibDataToJSON();
void settings_clear();

#endif