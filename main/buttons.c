#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"

#include "pins.h"
#include "eventgroup_defs.h"
#include "buttons.h"

static bool taskStop = false;

static int8_t buttonDebounceCount = 0;
static bool buttonDebouncedState = false;
static bool buttonIsPressed = false;
static bool buttonProcessed = false;
static TickType_t buttonPressTime;

static int8_t binSensorDebounceCount = 0;
static bool binSensorDebouncedState = false;
static uint8_t binSensorHystCount = 0;
static bool binSensorHystPrevState = false;
static bool binSensorState = false;

bool buttons_held()
{
  return (gpio_get_level(BUTTON_PIN) == 0);  // buttons are active-low
}

bool buttons_isBinPresent()
{
  return binSensorState;
}

void buttons_task(void *pvParameters)
{
  gpio_set_direction(BIN_IR_PIN, GPIO_MODE_INPUT);
  gpio_set_direction(BUTTON_PIN, GPIO_MODE_INPUT);
  gpio_pullup_en(BUTTON_PIN);

  ESP_LOGI(__func__, "Button task started on core %d", xPortGetCoreID());
  xEventGroupSetBits(xEventGroup, BUTTON_TASK_STARTED);

  while (!taskStop)
  {
    // Bin sensor debouncing (active high)
    if (gpio_get_level(BIN_IR_PIN) == 1)
    {
      if (++binSensorDebounceCount > 5)
      {
        binSensorDebounceCount = 5;
        binSensorDebouncedState = true;
      }
    }
    else
    {
      if (--binSensorDebounceCount < -5)
      {
        binSensorDebounceCount = -5;
        binSensorDebouncedState = false;
      }
    }

    // Bin sensor state variable hysteresis
    if (binSensorDebouncedState)
    {
      if (!binSensorHystPrevState) binSensorHystCount = 0;
      binSensorHystPrevState = true;

      // 2 sec delay to active
      // This is to make sure bin is settled before weight reading happens
      if (++binSensorHystCount > 200)
      {
        binSensorHystCount = 200;
        binSensorState = true;
      }
    }
    else
    {
      if (binSensorHystPrevState) binSensorHystCount = 0;
      binSensorHystPrevState = false;

      // 250ms delay to inactive
      if (++binSensorHystCount > 25)
      {
        binSensorHystCount = 25;
        binSensorState = false;
      }
    }

    // Button debouncing (buttons are active low)
    if (gpio_get_level(BUTTON_PIN) == 0)
    {
      if (++buttonDebounceCount > 5)
      {
        buttonDebounceCount = 5;
        buttonDebouncedState = true;
      }
    }
    else
    {
      if (--buttonDebounceCount < -5)
      {
        buttonDebounceCount = -5;
        buttonDebouncedState = false;
      }
    }

    // Button pressed/held processing
    if (buttonDebouncedState == true)
    {
      // Button just pressed
      if (!buttonIsPressed)
      {
        buttonPressTime = xTaskGetTickCount();
        buttonIsPressed = true;
      }
      else
      {
        // If Button has been held for LONG_PRESS_MS...
        if (!buttonProcessed && xTaskGetTickCount() - buttonPressTime > pdMS_TO_TICKS(LONG_PRESS_MS))
        {
          ESP_LOGI(__func__, "Button long press");
          xEventGroupSetBits(xEventGroup, BUTTON_LONG_PRESS);
          buttonPressTime = xTaskGetTickCount();
          buttonProcessed = true;
        }
      }
    }
    // Button just released
    else if (buttonIsPressed)
    {
      if (!buttonProcessed)
      {
        ESP_LOGI(__func__, "Button press");
        xEventGroupSetBits(xEventGroup, BUTTON_PRESS);
      }
      buttonIsPressed = false;
      buttonProcessed = false;
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
  }

  vTaskDelete(NULL);
}

void buttons_stop()
{
  taskStop = true;
}
