#ifndef UTIL_H
#define UTIL_H

#include "cJSON.h"

#define ACK_VAL              0  // I2C ack value
#define NACK_VAL             1  // I2C nack value
#define ACK_CHECK_EN         1  // I2C master will check ack from slave
#define ACK_CHECK_DIS        0  // I2C master will not check ack from slave

void spiffs_mount();
void i2c_init();
void syncTime();
int parse_hex_digit(char hex);
char* urldecode(const char* str, size_t len);
void cJSON_AddDecimalToObject(cJSON * const object, const char * const name, const float number, const uint8_t decimal_places);

#endif