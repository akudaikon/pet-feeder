#ifndef IS31FL3193_H
#define IS31FL3193_H

#define IS31FL3193_I2C_ADDR            0x68

#define IS31FL3193_REG_SHUTDOWN        0x00
#define IS31FL3193_REG_BREATHING_CTRL  0x01
#define IS31FL3193_REG_LED_MODE        0x02
#define IS31FL3193_REG_CUR_SETTING     0x03
#define IS31FL3193_REG_PWM             0x04
#define IS31FL3193_REG_DATA_UPDATE     0x07
#define IS31FL3193_REG_T0              0x0A
#define IS31FL3193_REG_T1_T2           0x10
#define IS31FL3193_REG_T3_T4           0x16
#define IS31FL3193_REG_TIME_UPDATE     0x1C
#define IS31FL3193_REG_LED_CTRL        0x1D
#define IS31FL3193_REG_RESET           0x2F

typedef enum {
  LED_FADE_TIME_130MS   = 0x00,
  LED_FADE_TIME_260MS   = 0x01,
  LED_FADE_TIME_520MS   = 0x02,
  LED_FADE_TIME_1004MS  = 0x03,
  LED_FADE_TIME_2008MS  = 0x04,
  LED_FADE_TIME_4160MS  = 0x05,
  LED_FADE_TIME_8320MS  = 0x06,
  LED_FADE_TIME_16640MS = 0x07
} LED_fade_times_t;

typedef enum {
  LED_ON_TIME_0MS       = 0x00,
  LED_ON_TIME_130MS     = 0x01,
  LED_ON_TIME_260MS     = 0x02,
  LED_ON_TIME_520MS     = 0x03,
  LED_ON_TIME_1004MS    = 0x04,
  LED_ON_TIME_2008MS    = 0x05,
  LED_ON_TIME_4160MS    = 0x06,
  LED_ON_TIME_8320MS    = 0x07,
  LED_ON_TIME_16640MS   = 0x08
} LED_on_times_t;

typedef enum {
  LED_OFF_TIME_0MS      = 0x00,
  LED_OFF_TIME_130MS    = 0x01,
  LED_OFF_TIME_260MS    = 0x02,
  LED_OFF_TIME_520MS    = 0x03,
  LED_OFF_TIME_1004MS   = 0x04,
  LED_OFF_TIME_2008MS   = 0x05,
  LED_OFF_TIME_4160MS   = 0x06,
  LED_OFF_TIME_8320MS   = 0x07,
  LED_OFF_TIME_16640MS  = 0x08,
  LED_OFF_TIME_33280MS  = 0x09,
  LED_OFF_TIME_66560MS  = 0x0A
} LED_off_times_t;

typedef enum {
  LED_CURRENT_42MA      = 0x00,
  LED_CURRENT_10MA      = 0x04,
  LED_CURRENT_8MA       = 0x08,
  LED_CURRENT_30MA      = 0x0C,
  LED_CURRENT_17MA      = 0x10,
} LED_currents_t;

void led_init();
void led_disableLEDs();
void led_setColor(uint8_t red, uint8_t green, uint8_t blue);
void led_enableBreathing(LED_off_times_t off_time, LED_on_times_t on_time, LED_fade_times_t fade_time);
void led_disableBreathing();
void led_setCurrent(LED_currents_t current);

#endif
