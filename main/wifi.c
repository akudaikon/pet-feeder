#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "esp_timer.h"

#include "wifi.h"
#include "settings.h"
#include "eventgroup_defs.h"

static esp_timer_handle_t timer_reconnect;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
  switch(event->event_id)
  {
    case SYSTEM_EVENT_STA_START:
      ESP_LOGI(__func__, "started");
    break;

    case SYSTEM_EVENT_STA_CONNECTED:
      ESP_LOGI(__func__, "connected");
    break;

    case SYSTEM_EVENT_STA_GOT_IP:
      strcpy(settings.ipAddress, ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
      xEventGroupSetBits(xEventGroup, WIFI_CONNECTED);
    break;

    case SYSTEM_EVENT_STA_STOP:
      ESP_LOGI(__func__, "stopped");
    break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
      ESP_LOGI(__func__, "disconnected. reason: %d", event->event_info.disconnected.reason);
      xEventGroupClearBits(xEventGroup, WIFI_CONNECTED);
      xEventGroupSetBits(xEventGroup, WIFI_DISCONNECTED);
      if (settings.configured) esp_timer_start_once(timer_reconnect, RECONNECT_DELAY * 1000); 
    break;

    default:
    break;
  }
  return ESP_OK;
}

esp_err_t wifi_init()
{
  esp_err_t error;
  tcpip_adapter_init();

  error = esp_event_loop_init(event_handler, NULL);
  if (error != ESP_OK) return error;

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  error = esp_wifi_init(&cfg);
  if (error != ESP_OK) return error;

  error = esp_wifi_set_storage(WIFI_STORAGE_RAM);
  if (error != ESP_OK) return error;

  esp_timer_create_args_t timerConfig;
  timerConfig.dispatch_method = ESP_TIMER_TASK;
  timerConfig.callback = (void*)esp_wifi_connect;
  timerConfig.arg = 0;
  timerConfig.name = "reconnect";
  esp_timer_create(&timerConfig, &timer_reconnect);

  return ESP_OK;
}

esp_err_t wifi_apStart()
{
  wifi_config_t apConfig =
  {
    .ap = {
      .ssid = "PetFeeder",
      .password = "",
      .ssid_len = 0,
      .authmode = WIFI_AUTH_OPEN,
      .ssid_hidden = 0,
      .max_connection = 2,
      .beacon_interval = 100
    }
  };
  apConfig.ap.channel = (esp_random() % 11) + 1;

  esp_wifi_set_mode(WIFI_MODE_APSTA);
  esp_wifi_set_config(WIFI_IF_AP, &apConfig);
  esp_wifi_start();

  return ESP_OK;
}

esp_err_t wifi_connect(char* ssid, char* password)
{
  wifi_config_t wifi_config;
  memset(&wifi_config, 0, sizeof(wifi_config));
  memcpy(wifi_config.sta.ssid, ssid, sizeof(wifi_config.sta.ssid));
  memcpy(wifi_config.sta.password, password, sizeof(wifi_config.sta.password));

  esp_wifi_set_mode(WIFI_MODE_STA);
  esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
  xEventGroupClearBits(xEventGroup, WIFI_CONNECTED | WIFI_DISCONNECTED);
  esp_wifi_start();
  esp_wifi_connect();

  ESP_LOGI(__func__, "Connecting to WiFi - %s %s", wifi_config.sta.ssid, wifi_config.sta.password);

  return ESP_OK;
}

esp_err_t wifi_waitUntilConnected()
{
  TickType_t lastTick = xTaskGetTickCount();
  while (!(xEventGroupGetBits(xEventGroup) & WIFI_CONNECTED))
  {
    if (xTaskGetTickCount() - lastTick > pdMS_TO_TICKS(CONNECT_TIMEOUT) || xEventGroupGetBits(xEventGroup) & WIFI_DISCONNECTED)
    {
      ESP_LOGI(__func__, "WiFi connect timed out");
      wifi_disconnect();
      return ESP_FAIL;
    }
    vTaskDelay(100 / portTICK_PERIOD_MS);
  };

  return ESP_OK;
}

void wifi_disconnect()
{
  esp_wifi_disconnect();
  esp_wifi_stop();

  ESP_LOGI(__func__, "Disconnected from WiFi");
}