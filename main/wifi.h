#ifndef WIFI_H
#define WIFI_H

#include "esp_err.h"

#define CONNECT_TIMEOUT   15000  // 15 sec
#define RECONNECT_DELAY   30000  // 30 sec

esp_err_t wifi_init();
esp_err_t wifi_apStart();
esp_err_t wifi_connect(char* ssid, char* password);
esp_err_t wifi_waitUntilConnected();
void wifi_disconnect();

#endif
