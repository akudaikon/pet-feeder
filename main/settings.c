#include <string.h>
#include "freertos/FreeRTOS.h"
#include "nvs_flash.h"
#include "settings.h"
#include "cJSON.h"
#include "esp_err.h"

esp_err_t nvs_init()
{
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES)
  {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  return ret;
}

esp_err_t nvs_get_string(nvs_handle handle, const char* key, char* out)
{
  size_t maxStrLength = MAX_STRING_SIZE;
  return nvs_get_str(handle, key, out, &maxStrLength);
}

esp_err_t settings_init()
{
  nvs_handle nvsHandle;
  esp_err_t err;

  // Open settings NVS space
  err = nvs_open(SETTINGS_NAMESPACE, NVS_READWRITE, &nvsHandle);
  assert(err == ESP_OK || err == ESP_ERR_NVS_NOT_FOUND);

  // Write default settings
  nvs_set_u8(nvsHandle, "configured", 0);
  nvs_set_u8(nvsHandle, "calibrated", 0);
  nvs_set_i32(nvsHandle, "calibBinEmpty", -1);
  nvs_set_i32(nvsHandle, "calibBinFull", -1);
  nvs_set_i32(nvsHandle, "calibBowlEmpty", -1);
  nvs_set_i32(nvsHandle, "calibBowlFood", -1);
  nvs_set_i32(nvsHandle, "calibWaterEmp", -1);
  nvs_set_i32(nvsHandle, "calibWaterFull", -1);
  nvs_set_u8(nvsHandle, "feedMode", 1);
  nvs_set_u8(nvsHandle, "feedAmount", 4);
  nvs_set_u8(nvsHandle, "missedFeed", 0);
  nvs_set_str(nvsHandle, "wifiSSID", "");
  nvs_set_str(nvsHandle, "wifiPass", "");
  nvs_set_u8(nvsHandle, "mqttEnable", 0);
  nvs_set_str(nvsHandle, "mqttURI", "");
  nvs_set_u8(nvsHandle, "timezone", 0);

  for (uint8_t x = 0; x < sizeof(settings.schedulerEvents) / sizeof(settings.schedulerEvents[0]); x++)
  {
    settings.schedulerEvents[x].enabled = false;
    settings.schedulerEvents[x].week_day = -1;
    settings.schedulerEvents[x].hour = 0;
    settings.schedulerEvents[x].minute = 0;
  }
  nvs_set_blob(nvsHandle, "scheduler", settings.schedulerEvents, sizeof(settings.schedulerEvents));

  // Commit settings to NVS
  err = nvs_commit(nvsHandle);
  assert(err == ESP_OK);

  // Finally, close NVS
  nvs_close(nvsHandle);
  return ESP_OK;
}

esp_err_t settings_read()
{
  nvs_handle nvsHandle;
  esp_err_t err;

  // Open settings NVS space
  err = nvs_open(SETTINGS_NAMESPACE, NVS_READONLY, &nvsHandle);
  if (err != ESP_OK)
  {
    nvs_close(nvsHandle);
    settings_init();
    nvs_open(SETTINGS_NAMESPACE, NVS_READONLY, &nvsHandle);
  }

  // Read settings from NVS
  nvs_get_u8(nvsHandle, "configured", &settings.configured);
  nvs_get_u8(nvsHandle, "calibrated", &settings.calibrated);
  nvs_get_i32(nvsHandle, "calibBinEmpty", &settings.calibration_bin_empty);
  nvs_get_i32(nvsHandle, "calibBinFull", &settings.calibration_bin_full);
  nvs_get_i32(nvsHandle, "calibBowlEmpty", &settings.calibration_bowl_empty);
  nvs_get_i32(nvsHandle, "calibBowlFood", &settings.calibration_bowl_food);
  nvs_get_i32(nvsHandle, "calibWaterEmp", &settings.calibration_water_empty);
  nvs_get_i32(nvsHandle, "calibWaterFull", &settings.calibration_water_full);
  nvs_get_u8(nvsHandle, "feedMode", &settings.feedMode);
  nvs_get_u8(nvsHandle, "feedAmount", &settings.feedAmount);
  nvs_get_u8(nvsHandle, "missedFeed", &settings.enableMissedFeed);
  nvs_get_string(nvsHandle, "wifiSSID", settings.wifiSSID);
  nvs_get_string(nvsHandle, "wifiPass", settings.wifiPassword);
  nvs_get_u8(nvsHandle, "mqttEnable", &settings.mqttEnable);
  nvs_get_string(nvsHandle, "mqttURI", settings.mqttURI);
  nvs_get_u8(nvsHandle, "timezone", &settings.timezone);

  size_t scheduleSize = sizeof(settings.schedulerEvents);
  nvs_get_blob(nvsHandle, "scheduler", &settings.schedulerEvents, &scheduleSize);

  // Finally, close NVS
  nvs_close(nvsHandle);

  return ESP_OK;
}

esp_err_t settings_write()
{
  nvs_handle nvsHandle;
  esp_err_t err;

  // Open settings NVS space
  err = nvs_open(SETTINGS_NAMESPACE, NVS_READWRITE, &nvsHandle);
  if (err != ESP_OK) return err;

  // Write settings
  nvs_set_u8(nvsHandle, "configured", settings.configured);
  nvs_set_u8(nvsHandle, "calibrated", settings.calibrated);
  nvs_set_i32(nvsHandle, "calibBinEmpty", settings.calibration_bin_empty);
  nvs_set_i32(nvsHandle, "calibBinFull", settings.calibration_bin_full);
  nvs_set_i32(nvsHandle, "calibBowlEmpty", settings.calibration_bowl_empty);
  nvs_set_i32(nvsHandle, "calibBowlFood", settings.calibration_bowl_food);
  nvs_set_i32(nvsHandle, "calibWaterEmp", settings.calibration_water_empty);
  nvs_set_i32(nvsHandle, "calibWaterFull", settings.calibration_water_full);
  nvs_set_u8(nvsHandle, "feedMode", settings.feedMode);
  nvs_set_u8(nvsHandle, "feedAmount", settings.feedAmount);
  nvs_set_u8(nvsHandle, "missedFeed", settings.enableMissedFeed);
  nvs_set_str(nvsHandle, "wifiSSID", settings.wifiSSID);
  nvs_set_str(nvsHandle, "wifiPass", settings.wifiPassword);
  nvs_set_u8(nvsHandle, "mqttEnable", settings.mqttEnable);
  nvs_set_str(nvsHandle, "mqttURI", settings.mqttURI);
  nvs_set_u8(nvsHandle, "timezone", settings.timezone);
  nvs_set_blob(nvsHandle, "scheduler", settings.schedulerEvents, sizeof(settings.schedulerEvents));

  // Commit settings to NVS
  err = nvs_commit(nvsHandle);
  if (err != ESP_OK) return err;

  // Finally, close NVS
  nvs_close(nvsHandle);
  return ESP_OK;
}

uint8_t settings_fromJSON(char* json)
{
  bool needRestart = false;
  cJSON *root = cJSON_Parse(json);

  if (root != NULL)
  {
    const cJSON *item = NULL;

    if (cJSON_HasObjectItem(root, "mqttEnable"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "mqttEnable");
      if (cJSON_IsBool(item))
      {
        bool prevVal = settings.mqttEnable;

        settings.mqttEnable = (cJSON_IsTrue(item) ? true : false);

        if (settings.mqttEnable != prevVal) needRestart = true;
      }
    }

    if (cJSON_HasObjectItem(root, "mqttURI"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "mqttURI");
      if (strcmp(settings.mqttURI, item->valuestring)) needRestart = true;
      strncpy(settings.mqttURI, item->valuestring, MAX_STRING_SIZE);
    }

    if (cJSON_HasObjectItem(root, "schedule"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "schedule");
      if (cJSON_GetArraySize(item) == 16)
      {
        // This is a terrible way of doing this, but oh well...
        settings.schedulerEvents[0].enabled = (cJSON_IsTrue(cJSON_GetArrayItem(item, 0)) ? true : false);
        settings.schedulerEvents[1].enabled = (cJSON_IsTrue(cJSON_GetArrayItem(item, 4)) ? true : false);
        settings.schedulerEvents[2].enabled = (cJSON_IsTrue(cJSON_GetArrayItem(item, 8)) ? true : false);
        settings.schedulerEvents[3].enabled = (cJSON_IsTrue(cJSON_GetArrayItem(item, 12)) ? true : false);

        settings.schedulerEvents[0].week_day = cJSON_GetArrayItem(item, 1)->valueint;
        settings.schedulerEvents[1].week_day = cJSON_GetArrayItem(item, 5)->valueint;
        settings.schedulerEvents[2].week_day = cJSON_GetArrayItem(item, 9)->valueint;
        settings.schedulerEvents[3].week_day = cJSON_GetArrayItem(item, 13)->valueint;

        settings.schedulerEvents[0].hour = cJSON_GetArrayItem(item, 2)->valueint;
        settings.schedulerEvents[1].hour = cJSON_GetArrayItem(item, 6)->valueint;
        settings.schedulerEvents[2].hour = cJSON_GetArrayItem(item, 10)->valueint;
        settings.schedulerEvents[3].hour = cJSON_GetArrayItem(item, 14)->valueint;

        settings.schedulerEvents[0].minute = cJSON_GetArrayItem(item, 3)->valueint;
        settings.schedulerEvents[1].minute = cJSON_GetArrayItem(item, 7)->valueint;
        settings.schedulerEvents[2].minute = cJSON_GetArrayItem(item, 11)->valueint;
        settings.schedulerEvents[3].minute = cJSON_GetArrayItem(item, 15)->valueint;
      }
    }

    if (cJSON_HasObjectItem(root, "feedMode"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "feedMode");
      if (cJSON_IsBool(item)) settings.feedMode = (cJSON_IsTrue(item) ? true : false);
    }

    if (cJSON_HasObjectItem(root, "feedAmount"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "feedAmount");
      if (cJSON_IsNumber(item)) settings.feedAmount = item->valueint;
    }

    if (cJSON_HasObjectItem(root, "enableMissedFeed"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "enableMissedFeed");
      if (cJSON_IsBool(item)) settings.enableMissedFeed = (cJSON_IsTrue(item) ? true : false);
    }

    if (cJSON_HasObjectItem(root, "timezone"))
    {
      item = cJSON_GetObjectItemCaseSensitive(root, "timezone");
      if (settings.timezone != item->valueint) needRestart = true;
      if (cJSON_IsNumber(item)) settings.timezone = item->valueint;
    }

    cJSON_Delete(root);

    if (needRestart) return SETTINGS_NEEDRESTART;
    else return SETTINGS_OK;
  }

  return SETTINGS_ERROR;
}

char* settings_toJSON()
{
  cJSON *root = NULL;
  cJSON *calibration = NULL;
  cJSON *schedule = NULL;
  root = cJSON_CreateObject();
  calibration = cJSON_CreateObject();
  schedule = cJSON_CreateObject();

  cJSON_AddBoolToObject(root, "configured", (settings.configured ? true : false));

  cJSON_AddItemToObject(root, "calibration", calibration);
  cJSON_AddBoolToObject(calibration, "binEmpty", ((settings.calibrated & SETTINGS_CALIBRATION_BIN_EMPTY) ? true : false));
  cJSON_AddBoolToObject(calibration, "binFull", ((settings.calibrated & SETTINGS_CALIBRATION_BIN_FULL) ? true : false));
  cJSON_AddBoolToObject(calibration, "bowlFood", ((settings.calibrated & SETTINGS_CALIBRATION_BOWL_FOOD) ? true : false));
  cJSON_AddBoolToObject(calibration, "waterEmpty", ((settings.calibrated & SETTINGS_CALIBRATION_WATER_EMPTY) ? true : false));
  cJSON_AddBoolToObject(calibration, "waterFull", ((settings.calibrated & SETTINGS_CALIBRATION_WATER_FULL) ? true : false));

  cJSON_AddStringToObject(root, "wifiSSID", settings.wifiSSID);
  cJSON_AddStringToObject(root, "wifiPassword", settings.wifiPassword);
  cJSON_AddBoolToObject(root, "mqttEnable", (settings.mqttEnable ? true : false));
  cJSON_AddStringToObject(root, "mqttURI", settings.mqttURI);

  schedule = cJSON_AddArrayToObject(root, "schedule");
  for (uint8_t i = 0; i < (sizeof(settings.schedulerEvents) / sizeof(settings.schedulerEvents[0])); i++)
  {
    cJSON_AddBoolToObject(schedule, "enabled", settings.schedulerEvents[i].enabled);
    cJSON_AddNumberToObject(schedule, "weekday", settings.schedulerEvents[i].week_day);
    cJSON_AddNumberToObject(schedule, "hour", settings.schedulerEvents[i].hour);
    cJSON_AddNumberToObject(schedule, "minute", settings.schedulerEvents[i].minute);
  }

  cJSON_AddNumberToObject(root, "timezone", settings.timezone);
  cJSON_AddBoolToObject(root, "feedMode", (settings.feedMode ? true : false));
  cJSON_AddNumberToObject(root, "feedAmount", settings.feedAmount);
  cJSON_AddBoolToObject(root, "enableMissedFeed", (settings.enableMissedFeed ? true : false));

  cJSON_AddStringToObject(root, "ip", settings.ipAddress);
  cJSON_AddBoolToObject(root, "mqttConnected", settings.mqttConnected);

  char *jsonSettings = cJSON_PrintUnformatted(root);
  cJSON_Delete(root);

  return jsonSettings;
}

char* settings_calibDataToJSON()
{
  cJSON *root = NULL;
  root = cJSON_CreateObject();

  cJSON_AddNumberToObject(root, "binEmpty", settings.calibration_bin_empty);
  cJSON_AddNumberToObject(root, "binFull", settings.calibration_bin_full);
  cJSON_AddNumberToObject(root, "bowlEmpty", settings.calibration_bowl_empty);
  cJSON_AddNumberToObject(root, "bowlFood", settings.calibration_bowl_food);
  cJSON_AddNumberToObject(root, "waterEmpty", settings.calibration_water_empty);
  cJSON_AddNumberToObject(root, "waterFull", settings.calibration_water_full);

  char *jsonSettings = cJSON_PrintUnformatted(root);
  cJSON_Delete(root);

  return jsonSettings;
}

void settings_clear()
{
  nvs_handle nvsHandle;
  if (nvs_open(SETTINGS_NAMESPACE, NVS_READONLY, &nvsHandle) != ESP_OK) return;
  nvs_erase_all(nvsHandle);
  nvs_close(nvsHandle);
  settings_init();
}
