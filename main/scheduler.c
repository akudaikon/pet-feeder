#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "esp_log.h"

#include "eventgroup_defs.h"
#include "settings.h"
#include "scheduler.h"

static bool taskStop = false;

void scheduler_task(void *pvParameters)
{
  ESP_LOGI(__func__, "Scheduler task started on core %d", xPortGetCoreID());
  xEventGroupSetBits(xEventGroup, SCHEDULER_TASK_STARTED);

  xEventGroupWaitBits(xEventGroup, TIME_SET, pdFALSE, pdFALSE, portMAX_DELAY);

  while (!taskStop)
  {
    time_t tnow = time(NULL);
    size_t num_events = sizeof(settings.schedulerEvents) / sizeof(settings.schedulerEvents[0]);
    struct tm now = {};

    localtime_r(&tnow, &now);

    // Check if any scheduler events are now
    if (settings.feedMode == SETTINGS_FEEDMODE_SCHEDULE)
    {
      for (uint8_t i = 0; i < num_events; i++)
      {
          scheduler_event_t *event = &settings.schedulerEvents[i];
          if (event->enabled == false) continue;
          if (event->week_day != -1 && event->week_day != now.tm_wday) continue;
          if (event->hour != now.tm_hour) continue;
          if (event->minute != now.tm_min) continue;
          if (now.tm_sec != 0) continue;

          // If so, set flag
          xEventGroupSetBits(xEventGroup, SCHEDULER_EVENT);
      }
    }

    // Set flag to signify end of day. Used to reset statistics
    if ((now.tm_hour == 23) && (now.tm_min == 59) && (now.tm_sec == 0)) xEventGroupSetBits(xEventGroup, END_OF_DAY_BEFORE);
    if ((now.tm_hour == 0) && (now.tm_min == 0) && (now.tm_sec == 0)) xEventGroupSetBits(xEventGroup, END_OF_DAY_AFTER);

    // Set flag to dim LED between 10pm and 8am
    if ((now.tm_hour >= 22) || (now.tm_hour <= 8))
    {
      // Set flag to dim LED if not already set
      if (!(xEventGroupGetBits(xEventGroup) & LED_DIM)) xEventGroupSetBits(xEventGroup, LED_DIM);
    }
    else
    {
      // Clear flag to dim LED if not already cleared
      if (xEventGroupGetBits(xEventGroup) & LED_DIM) xEventGroupClearBits(xEventGroup, LED_DIM);
    }

    vTaskDelay(750 / portTICK_PERIOD_MS);
  }

  vTaskDelete(NULL);
}

void scheduler_stop()
{
  taskStop = true;
}
