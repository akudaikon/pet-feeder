#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_timer.h"
#include "esp_task_wdt.h"
#include "driver/mcpwm.h"
#include "soc/mcpwm_periph.h"

#include "pins.h"
#include "motor.h"

#define MOTOR_PWM_FREQUENCY   10000 // 10kHz
#define MOTOR_PWM_DUTY        50.0  // 50%

#define MOTOR_RAMP_TIME       2000  // 2 sec
#define MOTOR_STEP_SIZE       5.0   // 5%

#define MOTOR_STEP_TIME       ((MOTOR_RAMP_TIME / ((100 - MOTOR_PWM_DUTY) / MOTOR_STEP_SIZE)) * 1000)

static esp_timer_handle_t ramp_timer;
static bool motor_running = false;

static void motor_ramp()
{
  float duty = mcpwm_get_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A) - MOTOR_STEP_SIZE;

  if (duty < MOTOR_PWM_DUTY)
  {
    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, MOTOR_PWM_DUTY);
  }
  else
  {
    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, duty);
    esp_timer_start_once(ramp_timer, MOTOR_STEP_TIME);
  } 
}

void motor_run()
{
  if (!motor_running)
  {
    mcpwm_set_signal_high(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A);
    mcpwm_set_signal_high(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B);
    gpio_set_level(DRV8837_NSLEEP_PIN, 1);
    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, 100.0);
    mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, MCPWM_DUTY_MODE_0);
    esp_timer_start_once(ramp_timer, MOTOR_STEP_TIME);
    motor_running = true;
  }
}

void motor_stop()
{
  esp_timer_stop(ramp_timer);

  if (motor_running)
  {
    // PWM brake for 90ms
    mcpwm_set_signal_high(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A);
    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B, 50.0);
    mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B, MCPWM_DUTY_MODE_0);
    vTaskDelay(90 / portTICK_PERIOD_MS);

    // turn off motor
    mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A);
    mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B);
    motor_running = false;
  }

  gpio_set_level(DRV8837_NSLEEP_PIN, 0);
}

bool motor_waitForRotationSensor(uint8_t transitions, uint16_t timeout_ms)
{
  TickType_t timeout;
  bool sensor_state = gpio_get_level(BIN_ROT_SENSOR_PIN);

  // Four transitions = 1/4 rotation
  for (uint8_t i = 0; i < transitions; i++)
  {
    // Wait for rotation sensor to change state
    timeout = xTaskGetTickCount();
    while (gpio_get_level(BIN_ROT_SENSOR_PIN) == sensor_state)
    {
      if ((xTaskGetTickCount() - timeout) > pdMS_TO_TICKS(timeout_ms)) return false;
      vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    sensor_state = gpio_get_level(BIN_ROT_SENSOR_PIN);
  }

  return true;
}

void motor_init()
{
  gpio_set_direction(BIN_ROT_SENSOR_PIN, GPIO_MODE_INPUT);
  gpio_set_direction(DRV8837_NSLEEP_PIN, GPIO_MODE_OUTPUT);
  gpio_set_level(DRV8837_NSLEEP_PIN, 0);

  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, DRV8837_IN1_PIN);
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0B, DRV8837_IN2_PIN);

  mcpwm_config_t pwm_config;
  pwm_config.frequency = MOTOR_PWM_FREQUENCY;
  pwm_config.cmpr_a = 0;
  pwm_config.cmpr_b = 0;
  pwm_config.counter_mode = MCPWM_UP_COUNTER;
  pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
  mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);

  mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A);
  mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B);

  esp_timer_create_args_t timer_config;
  timer_config.dispatch_method = ESP_TIMER_TASK;
  timer_config.callback = (void*)motor_ramp;
  timer_config.arg = 0;
  timer_config.name = "ramp_timer";
  esp_timer_create(&timer_config, &ramp_timer);
}
