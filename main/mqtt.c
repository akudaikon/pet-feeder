#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "eventgroup_defs.h"
#include "mqtt_client.h"
#include "settings.h"
#include "util.h"
#include "stats.h"
#include "mqtt.h"

esp_mqtt_client_handle_t mqttClient = NULL;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
  switch (event->event_id)
  {
    case MQTT_EVENT_CONNECTED:
      xEventGroupSetBits(xEventGroup, MQTT_CONNECTED);
      esp_mqtt_client_publish(mqttClient, "petfeeder/availability", "online", 0, 0, 1);
      settings.mqttConnected = true;
      break;

    case MQTT_EVENT_DISCONNECTED:
      xEventGroupClearBits(xEventGroup, MQTT_CONNECTED);
      settings.mqttConnected = false;
      break;

    case MQTT_EVENT_DATA:
      ESP_LOGI(__func__, "%.*s: %.*s", event->topic_len, event->topic, event->data_len, event->data);
      break;

    case MQTT_EVENT_ERROR:
      ESP_LOGI(__func__, "MQTT_EVENT_ERROR");
      break;

    default:
      break;
  }
  return ESP_OK;
}

void mqtt_sendUpdate()
{
  if (!settings.mqttEnable) return;
  if (!(xEventGroupGetBits(xEventGroup) & WIFI_CONNECTED)) return;
  if (!(xEventGroupGetBits(xEventGroup) & MQTT_CONNECTED)) return;

  char *mqttUpdate = stats_toJSON();
  ESP_LOGI(__func__, "Sending MQTT update: %s", mqttUpdate);
  esp_mqtt_client_publish(mqttClient, "petfeeder/state", mqttUpdate, 0, 0, 0);
  free(mqttUpdate);
}

esp_err_t mqtt_start(char* uri)
{
  if (settings.mqttURI[0] == '\0') return ESP_FAIL;

  esp_mqtt_client_config_t mqtt_cfg =
  {
    .event_handle = mqtt_event_handler,
    .lwt_topic = "petfeeder/availability",
    .lwt_msg = "offline",
    .lwt_msg_len = 15,
    .lwt_retain = true,
    .uri = uri,
  };

  ESP_LOGI(__func__, "Connecting to MQTT: %s", mqtt_cfg.uri);
  mqttClient = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_start(mqttClient);
  xEventGroupSetBits(xEventGroup, MQTT_CLIENT_STARTED);

  return ESP_OK;
}

void mqtt_stop()
{
  if (mqttClient != NULL)
  {
    esp_mqtt_client_stop(mqttClient);
    ESP_LOGI(__func__, "MQTT stopped");
    mqttClient = NULL;
  }
}
