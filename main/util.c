#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_spiffs.h"
#include "esp_log.h"
#include "esp_sntp.h"
#include "cJSON.h"
#include "eventgroup_defs.h"
#include "pins.h"
#include "settings.h"

void spiffs_mount()
{
  esp_vfs_spiffs_conf_t spiffs_conf =
  {
    .base_path = "",
    .partition_label = NULL,
    .max_files = 5,
    .format_if_mount_failed = false
  };

  esp_err_t ret = esp_vfs_spiffs_register(&spiffs_conf);

  if (ret != ESP_OK)
  {
    if (ret == ESP_FAIL) { ESP_LOGE(__func__, "Failed to mount or format filesystem\n"); }
    else if (ret == ESP_ERR_NOT_FOUND) { ESP_LOGE(__func__, "Failed to find SPIFFS partition\n"); }
    else { ESP_LOGE(__func__, "Failed to initialize SPIFFS (%d)\n", ret); }
  }

  ESP_LOGI(__func__, "SPIFFS mounted");
  size_t total = 0, used = 0;
  esp_spiffs_info(NULL, &total, &used);
  ESP_LOGI(__func__, "SPIFFS total: %d, used: %d", total, used);
}

void i2c_init()
{
  i2c_config_t i2c_conf =
  {
    .mode = I2C_MODE_MASTER,
    .sda_io_num = I2C_SDA_PIN,
    .scl_io_num = I2C_SCL_PIN,
    .sda_pullup_en = GPIO_PULLUP_ENABLE,
    .scl_pullup_en = GPIO_PULLUP_ENABLE,
    .master.clk_speed = I2C_FREQ_HZ
  };

  i2c_param_config(I2C_PORT_NUM, &i2c_conf);
  i2c_driver_install(I2C_PORT_NUM, i2c_conf.mode, 0, 0, 0);
}

int parse_hex_digit(char hex)
{
  switch (hex)
  {
    case '0' ... '9': return hex - '0';
    case 'a' ... 'f': return hex - 'a' + 0xa;
    case 'A' ... 'F': return hex - 'A' + 0xA;
    default:          return -1;
  }
}

void syncTime()
{
  sntp_setoperatingmode(SNTP_OPMODE_POLL);
  sntp_setservername(0, "pool.ntp.org");
  sntp_init();

  switch (settings.timezone)
  {
    default:
    case 0:  // Eastern
      setenv("TZ", "EST5EDT", 1);
    break;

    case 1:  // Central
      setenv("TZ", "CST6CDT", 1);
    break;

    case 2:  // Mountain
      setenv("TZ", "MST7MDT", 1);
    break;

    case 3:  // Pacific
      setenv("TZ", "PST8PDT", 1);
    break;
  }

  tzset();

  int retry_count = 1;
  time_t now = 0;
  struct tm timeinfo = { 0 };

  time(&now);
  localtime_r(&now, &timeinfo);

  while (timeinfo.tm_year < (2016 - 1900))
  {
    ESP_LOGI(__func__, "Waiting for time to be set... (attempt %d)", retry_count);
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    retry_count++;
    time(&now);
    localtime_r(&now, &timeinfo);
  }

  char strftime_buf[64];
  strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
  ESP_LOGI(__func__, "Got time: %s", strftime_buf);

  xEventGroupSetBits(xEventGroup, TIME_SET);
}

char* urldecode(const char* str, size_t len)
{
  const char* end = str + len;
  char* out = calloc(1, len + 1);
  char* p_out = out;
  while (str != end)
  {
    char c = *str++;
    if (c == '+') *p_out = ' ';
    else if (c != '%') *p_out = c;
    else
    {
      if (str + 2 > end) return NULL;  // Unexpected end of string
      int high = parse_hex_digit(*str++);
      int low = parse_hex_digit(*str++);
      if (high == -1 || low == -1) return NULL;  // Unexpected character
      *p_out = high * 16 + low;
    }
    ++p_out;
  }
  *p_out = 0;
  return out;
}

void cJSON_AddDecimalToObject(cJSON * const object, const char * const name, const float number, const uint8_t decimal_places)
{
  char format[7];
  char buf[12];
  sprintf(format, "%%.%df", decimal_places);
  sprintf(buf, format, number);
  cJSON_AddRawToObject(object, name, buf);
}