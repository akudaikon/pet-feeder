#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/i2c.h"

#include "pins.h"
#include "util.h"
#include "IS31FL3193.h"

static LED_off_times_t old_offTime = 0xFF;
static LED_on_times_t old_onTime = 0xFF;
static LED_fade_times_t old_fadeTime = 0xFF;

static void IS31FL3193_writeByte(uint8_t reg_addr, uint8_t data)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (IS31FL3193_I2C_ADDR << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, data, ACK_CHECK_EN);
  i2c_master_stop(cmd);
  i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
}

static void IS31FL3193_writeBytes(uint8_t reg_addr, uint8_t data1, uint8_t data2, uint8_t data3)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (IS31FL3193_I2C_ADDR << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, data1, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, data2, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, data3, ACK_CHECK_EN);
  i2c_master_stop(cmd);
  i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
}

void led_init()
{
  ESP_LOGI(__func__, "IS31FL3193 init start");

  IS31FL3193_writeByte(IS31FL3193_REG_RESET, 0x00);               // reset registers
  IS31FL3193_writeByte(IS31FL3193_REG_SHUTDOWN, 0x20);            // all channel enable, normal operation
  IS31FL3193_writeByte(IS31FL3193_REG_BREATHING_CTRL, 0x00);      // disabled
  IS31FL3193_writeByte(IS31FL3193_REG_LED_MODE, 0x00);            // PWM mode
  IS31FL3193_writeByte(IS31FL3193_REG_CUR_SETTING, 0x0C);         // 30mA
  IS31FL3193_writeByte(IS31FL3193_REG_LED_CTRL, 0x00);            // all LEDs off
  IS31FL3193_writeBytes(IS31FL3193_REG_T0, 0x00, 0x00, 0x00);     // OUT1, OUT2, OUT3
  IS31FL3193_writeBytes(IS31FL3193_REG_T1_T2, 0x00, 0x00, 0x00);  // OUT1, OUT2, OUT3
  IS31FL3193_writeBytes(IS31FL3193_REG_T3_T4, 0x00, 0x00, 0x00);  // OUT1, OUT2, OUT3
  IS31FL3193_writeByte(IS31FL3193_REG_TIME_UPDATE, 0x00);         // push timing updates
  ESP_LOGI(__func__, "IS31FL3193 init done");
}

void led_disableLEDs()
{
  IS31FL3193_writeByte(IS31FL3193_REG_LED_CTRL, 0x00);            // all LEDs off
}

void led_setColor(uint8_t red, uint8_t green, uint8_t blue)
{
  IS31FL3193_writeBytes(IS31FL3193_REG_PWM, red, green, blue);
  IS31FL3193_writeByte(IS31FL3193_REG_DATA_UPDATE, 0x00);         // push PWM updates
  IS31FL3193_writeByte(IS31FL3193_REG_LED_CTRL, 0x07);            // all LEDs on
}

void led_enableBreathing(LED_off_times_t off_time, LED_on_times_t on_time, LED_fade_times_t fade_time)
{
  // If breathing already enabled with same params, don't set it again
  // This stops the breathing animation from being interrupted
  if (off_time == old_offTime && on_time == old_onTime && fade_time == old_fadeTime) return;

  old_offTime = off_time;
  old_onTime = on_time;
  old_fadeTime = fade_time;

  IS31FL3193_writeByte(IS31FL3193_REG_LED_CTRL, 0x00);            // all LEDs off
  IS31FL3193_writeByte(IS31FL3193_REG_BREATHING_CTRL, 0x00);      // enable breathing
  IS31FL3193_writeByte(IS31FL3193_REG_LED_MODE, 0x20);            // one shot mode

  IS31FL3193_writeBytes(IS31FL3193_REG_T0,                        // no start delay
                        0x00,                                     // OUT1
                        0x00,                                     // OUT2
                        0x00);                                    // OUT3

  IS31FL3193_writeBytes(IS31FL3193_REG_T1_T2,
                        ((fade_time & 0x7) << 5) | ((on_time & 0x0F) << 1),    // OUT1
                        ((fade_time & 0x7) << 5) | ((on_time & 0x0F) << 1),    // OUT2
                        ((fade_time & 0x7) << 5) | ((on_time & 0x0F) << 1));   // OUT3

  IS31FL3193_writeBytes(IS31FL3193_REG_T3_T4,
                        ((fade_time & 0x7) << 5) | ((off_time & 0x0F) << 1),   // OUT1
                        ((fade_time & 0x7) << 5) | ((off_time & 0x0F) << 1),   // OUT2
                        ((fade_time & 0x7) << 5) | ((off_time & 0x0F) << 1));  // OUT3

  IS31FL3193_writeByte(IS31FL3193_REG_TIME_UPDATE, 0x00);         // push timing updates
}

void led_disableBreathing()
{
  old_offTime = 0xFF;
  old_onTime = 0xFF;
  old_fadeTime = 0xFF;

  IS31FL3193_writeByte(IS31FL3193_REG_LED_MODE, 0x00);            // PWM mode
}

void led_setCurrent(LED_currents_t current)
{
  IS31FL3193_writeByte(IS31FL3193_REG_CUR_SETTING, current);
}
