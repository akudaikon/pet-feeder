#ifndef BUTTONS_H
#define BUTTONS_H

#define LONG_PRESS_MS   1000

bool buttons_held();
bool buttons_isBinPresent();
void buttons_task(void *pvParameters);
void buttons_stop();

#endif