var binEmptyBtn = document.getElementById('binEmptyBtn'),
    binFullBtn = document.getElementById('binFullBtn'),
    bowlFoodBtn = document.getElementById('bowlFoodBtn'),
    continueBtn = document.getElementById('continueBtn'),
    binEmptyGlyph = document.getElementById('binEmptyGlyph'),
    binFullGlyph = document.getElementById('binFullGlyph'),
    bowlEmptyGlyph = document.getElementById('bowlEmptyGlyph'),
    bowlFoodGlyph = document.getElementById('bowlFoodGlyph'),
    messageModalIcon = document.getElementById('messageModalIcon'),
    messageModalText = document.getElementById('messageModalText'),
    messageModalProgress = document.getElementById('messageModalProgress');

getSettings = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = JSON.parse(xhttp.responseText);

        binEmptyBtn.innerHTML = (settings.calibration.binEmpty ? "Recalibrate" : "Calibrate");
        binFullBtn.innerHTML = (settings.calibration.binFull ? "Recalibrate" : "Calibrate");
        bowlFoodBtn.innerHTML = (settings.calibration.bowlFood ? "Recalibrate" : "Calibrate");

        binEmptyGlyph.className = (settings.calibration.binEmpty ? "glyphicon glyphicon-ok glyph-good" : "glyphicon glyphicon-remove glyph-bad");
        binFullGlyph.className = (settings.calibration.binFull ? "glyphicon glyphicon-ok glyph-good" : "glyphicon glyphicon-remove glyph-bad");
        bowlFoodGlyph.className = (settings.calibration.bowlFood ? "glyphicon glyphicon-ok glyph-good" : "glyphicon glyphicon-remove glyph-bad");

        if (settings.calibration.binEmpty && settings.calibration.binFull && settings.calibration.bowlFood)
        {
          $("#continueBtn").removeAttr("disabled");
        }
      } else {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error reading calibration!</h4>Please refresh and try again<br>(Error " + xhttp.status + ")<br>";
        messageModalProgress.style.display = "none";
        $('#messageModal').modal('show');
      }
    }
  };

  xhttp.open("GET", "action?getsettings", true);
  xhttp.send();
};

var iter = 0;

doCalib = function(type) {
  var xhttp = new XMLHttpRequest();
  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-time'></span></h1>";

  if (type == 'binEmpty')
  {
    if (confirm("Ensure food bin is inserted and empty, then click OK to start calibration."))
    {
      iter = 0;
      messageModalText.innerHTML = "<h4>Doing Empty Food Bin Calibration...</h4>Please wait";
      messageModalProgress.style.display = "block";
      $('#messageModal').modal('show');

      xhttp.open("GET", "action?calib=binEmpty", false);
      xhttp.send();

      (function poll() {
        setTimeout(function() {
          $.ajax({
            url: "action?getsettings",
            type: "GET",
            success: function(settings) {
              if (settings.calibration.binEmpty == true)
              {
                getSettings();
                $('#messageModal').modal('hide');
              }
              else
              {
                if (++iter < 10) poll();
                else
                {
                  getSettings();
                  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
                  messageModalText.innerHTML = "<h4>Empty Food Bin Calibration Failed!</h4>Ensure food bin is inserted and empty,<br>then try again<br>";
                  messageModalProgress.style.display = "none";
                  setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
                }
              }
            },
            error: function(e) {
              getSettings();
              messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
              messageModalText.innerHTML = "<h4>Full Food Bin Calibration Failed!</h4>Please try again<br>(Error " + e.responseText + ")<br>";
              messageModalProgress.style.display = "none";
              setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
            },
            dataType: "json",
            timeout: 2000
          })
        }, 2000);
      })();
    }
  }
  else if (type == 'binFull')
  {
    if (confirm("Ensure food bin is inserted and filled fully, then click OK to start calibration."))
    {
      iter = 0;
      messageModalText.innerHTML = "<h4>Doing Full Food Bin Calibration...</h4>Please wait";
      messageModalProgress.style.display = "block";
      $('#messageModal').modal('show');

      xhttp.open("GET", "action?calib=binFull", false);
      xhttp.send();

      (function poll() {
        setTimeout(function() {
          $.ajax({
            url: "action?getsettings",
            type: "GET",
            success: function(settings) {
              if (settings.calibration.binFull == true)
              {
                getSettings();
                $('#messageModal').modal('hide');
              }
              else
              {
                if (++iter < 10) poll();
                else
                {
                  getSettings();
                  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
                  messageModalText.innerHTML = "<h4>Full Food Bin Calibration Failed!</h4>Ensure food bin is inserted and full,<br>then try again<br>";
                  messageModalProgress.style.display = "none";
                  setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
                }
              }
            },
            error: function(e) {
              getSettings();
              messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
              messageModalText.innerHTML = "<h4>Full Food Bin Calibration Failed!</h4>Please try again<br>(Error " + e.responseText + ")<br>";
              messageModalProgress.style.display = "none";
              setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
            },
            dataType: "json",
            timeout: 2000
          })
        }, 2000);
      })();
    }
  }
  else if (type == 'bowlFood')
  {
    if (confirm("Ensure food bowl is empty and there is food in the bin, then click OK to start calibration. Calibration will dispense a certain amount of food into the bowl."))
    {
      iter = 0;
      messageModalText.innerHTML = "<h4>Doing Food Bowl Calibration...</h4>Please wait";
      messageModalProgress.style.display = "block";
      $('#messageModal').modal('show');

      xhttp.open("GET", "action?calib=bowlFood", false);
      xhttp.send();

      (function poll() {
        setTimeout(function() {
          $.ajax({
            url: "action?getsettings",
            type: "GET",
            success: function(settings) {
              if (settings.calibration.bowlFood == true)
              {
                getSettings();
                $('#messageModal').modal('hide');
              }
              else
              {
                if (++iter < 60) poll();
                else
                {
                  getSettings();
                  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
                  messageModalText.innerHTML = "<h4>Food Bowl Calibration Failed!</h4>Make sure there's food in the food bin, empty food bowl,<br>then try again<br>";
                  messageModalProgress.style.display = "none";
                  setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
                }
              }
            },
            error: function(e) {
              getSettings();
              messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
              messageModalText.innerHTML = "<h4>Food Bowl Calibration Failed!</h4>Please try again<br>(Error " + e.responseText + ")<br>";
              messageModalProgress.style.display = "none";
              setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
            },
            dataType: "json",
            timeout: 2000
          })
        }, 2000);
      })();
    }
  }
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);