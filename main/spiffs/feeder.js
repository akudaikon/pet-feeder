var mqttEnable = document.getElementById('mqttEnable'),
    mqttURI = document.getElementById('mqttURI'),
    feedMode = document.getElementById('feedMode'),
    schedModeDiv = document.getElementById('schedModeDiv'),
    footer = document.getElementById('footerDiv'),
    feedNowBtn = document.getElementById('feedNowBtn'),
    saveSettingsBtn = document.getElementById('saveSettingsBtn'),
    messageModalIcon = document.getElementById('messageModalIcon'),
    messageModalText = document.getElementById('messageModalText'),
    messageModalProgress = document.getElementById('messageModalProgress');

feedMode.onchange = function() {
  schedModeDiv.style.display = (this.checked ? 'block' : 'none');
};

mqttEnable.onchange = function() {
  mqttDiv.style.display = (this.checked ? 'block' : 'none');
};

pad2 = function(number) {  
  return (number < 10 ? '0' : '') + number
}

getSettings = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = JSON.parse(xhttp.responseText);

        document.getElementById("sched_1_enable").checked = settings.schedule[0];
        document.getElementById("sched_2_enable").checked = settings.schedule[4];
        document.getElementById("sched_3_enable").checked = settings.schedule[8];
        document.getElementById("sched_4_enable").checked = settings.schedule[12];

        document.getElementById("sched_1_day").value = settings.schedule[1];
        document.getElementById("sched_2_day").value = settings.schedule[5];
        document.getElementById("sched_3_day").value = settings.schedule[9];
        document.getElementById("sched_4_day").value = settings.schedule[13];

        document.getElementById("sched_1_time").value = pad2(settings.schedule[2])  + ":" + pad2(settings.schedule[3]);
        document.getElementById("sched_2_time").value = pad2(settings.schedule[6])  + ":" + pad2(settings.schedule[7]);
        document.getElementById("sched_3_time").value = pad2(settings.schedule[10]) + ":" + pad2(settings.schedule[11]);
        document.getElementById("sched_4_time").value = pad2(settings.schedule[14]) + ":" + pad2(settings.schedule[15]);

        document.getElementById("timezone").value = settings.timezone;
        
        document.getElementById("feedAmount").value = settings.feedAmount;
        document.getElementById("enableMissedFeed").checked = settings.enableMissedFeed;

        feedMode.checked = settings.feedMode;
        schedModeDiv.style.display = (feedMode.checked ? 'block' : 'none');

        mqttEnable.checked = settings.mqttEnable;
        mqttURI.value = settings.mqttURI;
        mqttDiv.style.display = (mqttEnable.checked ? 'block' : 'none');

        footer.innerHTML = "<small>MQTT: " + (settings.mqttConnected ? "Connected" : "Not Connected") + "<br>IP Address: " + settings.ip + "</small>";

        $('#messageModal').modal('hide');
      } else {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error reading settings!</h4>Please refresh and try again<br>(Error " + xhttp.status + ")<br>";
        messageModalProgress.style.display = "none";
      }
    }
  };

  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-hourglass'></span></h1>";
  messageModalText.innerHTML = "<h4>Getting Settings...</h4>";
  messageModalProgress.style.display = "block";
  $('#messageModal').modal('show');

  xhttp.open("GET", "action?getsettings", true);
  xhttp.send();
};

saveSettingsBtn.onclick = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-ok' style='color: green'></span></h1>";
        messageModalText.innerHTML = "<h4>Settings saved successfully!</h4>";
        messageModalProgress.style.display = "none";
        setTimeout(function() { $('#messageModal').modal('hide'); }, 2000);
      } else if (xhttp.status == 202) {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-ok' style='color: green'></span></h1>";
        messageModalText.innerHTML = "<h4>Settings saved successfully!</h4>Pet feeder will now restart for settings to take effect.<br>Please refresh after restart completed.<br>";
      } else {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error saving settings!</h4>Please try again<br>(Error " + xhttp.status + ")<br>";
        messageModalProgress.style.display = "none";
        setTimeout(function() { $('#messageModal').modal('hide'); }, 2000);
      }
    }
  };

  if (document.getElementById("sched_1_time").value == "") document.getElementById("sched_1_time").value = "00:00:00";
  if (document.getElementById("sched_2_time").value == "") document.getElementById("sched_2_time").value = "00:00:00";
  if (document.getElementById("sched_3_time").value == "") document.getElementById("sched_3_time").value = "00:00:00";
  if (document.getElementById("sched_4_time").value == "") document.getElementById("sched_4_time").value = "00:00:00";

  var settings = JSON.stringify(
    { 
      "schedule": [document.getElementById("sched_1_enable").checked, 
                   parseInt(document.getElementById("sched_1_day").value),
                   parseInt(document.getElementById("sched_1_time").value.split(":")[0]),
                   parseInt(document.getElementById("sched_1_time").value.split(":")[1]),
                   document.getElementById("sched_2_enable").checked, 
                   parseInt(document.getElementById("sched_2_day").value),
                   parseInt(document.getElementById("sched_2_time").value.split(":")[0]),
                   parseInt(document.getElementById("sched_2_time").value.split(":")[1]),
                   document.getElementById("sched_3_enable").checked, 
                   parseInt(document.getElementById("sched_3_day").value),
                   parseInt(document.getElementById("sched_3_time").value.split(":")[0]),
                   parseInt(document.getElementById("sched_3_time").value.split(":")[1]),
                   document.getElementById("sched_4_enable").checked, 
                   parseInt(document.getElementById("sched_4_day").value),
                   parseInt(document.getElementById("sched_4_time").value.split(":")[0]),
                   parseInt(document.getElementById("sched_4_time").value.split(":")[1])],
      "feedAmount": parseInt(document.getElementById("feedAmount").value),
      "feedMode": feedMode.checked,
      "enableMissedFeed": enableMissedFeed.checked,
      "timezone": parseInt(document.getElementById("timezone").value),
      "mqttEnable": mqttEnable.checked,
      "mqttURI": mqttURI.value,
    }
  );
  console.log(settings);

  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-hourglass'></span></h1>";
  messageModalText.innerHTML = "<h4>Saving settings...</h4>";
  messageModalProgress.style.display = "block";
  $('#messageModal').modal('show');

  xhttp.open("POST", "action?savesettings", true);
  xhttp.send(settings);
}

feedNowBtn.onclick = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-download-alt' style='color: black'></span></h1>";
        messageModalText.innerHTML = "<h4>Dispensing food...</h4>";
        messageModalProgress.style.display = "block";
      } else if (xhttp.status == 202) {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-info-sign' style='color: blue'></span></h1>";
        messageModalText.innerHTML = "<h4>Food bowl is already full!</h4>";
        messageModalProgress.style.display = "none";
      } else {
        messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
        messageModalText.innerHTML = "<h4>Error communicating with Pet Feeder!</h4>Please try again<br>(Error " + xhttp.status + ")<br>";
        messageModalProgress.style.display = "none";
      }

      $('#messageModal').modal('show');
      setTimeout(function() { $('#messageModal').modal('hide'); }, 2000);
    }
  };

  xhttp.open("GET", "action?feednow", true);
  xhttp.send();
}

getStats = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var stats = JSON.parse(xhttp.responseText);

        document.getElementById("bowlAmount").innerHTML = stats.bowlAmount + " cup" + (stats.bowlAmount != 1 ? "s" : "");
        document.getElementById("binPercent").innerHTML = stats.binPercent + "%";
        document.getElementById("binProgress").style = "width: " + stats.binPercent + "%";

        document.getElementById("binProgress").className = "progress-bar";
        if (stats.binPercent > 50) document.getElementById("binProgress").classList.add("progress-bar-success");
        else if (stats.binPercent > 25) document.getElementById("binProgress").classList.add("progress-bar-warning");
        else document.getElementById("binProgress").classList.add("progress-bar-danger");

        document.getElementById("foodGiven").innerHTML = stats.foodGiven + " cup" + (stats.foodGiven != 1 ? "s" : "");
        document.getElementById("foodEaten").innerHTML = stats.foodEaten + " cup" + (stats.foodEaten != 1 ? "s" : "");

        setTimeout(getStats, 5000);
      }
    }
  };

  xhttp.open("GET", "action?getstats", true);
  xhttp.send();
};

resetStats = function() {
  if (confirm("Click OK to verify that you want to reset today's statistics"))
  {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "action?resetstats", false);
    xhttp.send();
  }
}

$('.panel-collapse').on('show.bs.collapse', function (e) {
  getSettings();
  $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
  $(this).siblings('.panel-heading').removeClass('active');
});

// To get around onLoad not firing on back
setTimeout(getStats, 100);
