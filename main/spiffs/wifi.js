getWifiNetworks = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var ssidList = JSON.parse(xhttp.responseText);

        if (ssidList.length == 0) {
          document.getElementById('ssidList').innerHTML = "No Wifi networks found!";
        } else {
          document.getElementById('ssidList').innerHTML = "";
          var tbl = document.createElement('table');

          for (var i = 0; i < ssidList.length - 1; i++) {
            var tr = tbl.insertRow();
            tr.insertCell().innerHTML = "<a href=\"javascript:setSSID(\'" + ssidList[i].ssid + "\')\">" + ssidList[i].ssid + "</a>";
            tr.insertCell().innerHTML = ssidList[i].rssi;

            if (ssidList[i].auth == true) {
              tr.insertCell().innerHTML = "<td><img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAMAAAC67D+PAAAADFBMVEUAAAAAtgD///8AAADV1YIHAAAAA3RSTlMAAAD6dsTeAAAAHUlEQVR4AWMAAmYgYGBkArHAAMoEYnQmMxSQywQAKvMA3+mlSFAAAAAASUVORK5CYII='/></td>";
            } else {
              tr.insertCell().innerHTML = "";
            }

          }
          document.getElementById('ssidList').appendChild(tbl);
        }
      }
    }
  };
  xhttp.open("GET", "action?wifiscan", true);
  xhttp.send();
}

setSSID = function(ssid)
{
  document.getElementById('wifiSSID').value = ssid;
  document.getElementById('wifiPassword').value = "";
  document.getElementById('wifiPassword').focus();
}

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = JSON.parse(xhttp.responseText);

        document.getElementById('wifiSSID').value = settings.wifiSSID;
        document.getElementById('wifiPassword').value = settings.wifiPassword;

        disableForm(document.getElementById('settingsForm'), false);
      }
    }
  };
  xhttp.open("GET", "action?getsettings", true);
  xhttp.send();
}

function disableForm(form, enable)
{
  var elements = form.elements;
  for (var i = 0, len = elements.length; i < len; ++i) elements[i].disabled = enable;
}

// To get around onLoad not firing on back
disableForm(document.getElementById('settingsForm'), true);
setTimeout(getSettings, 100);
setTimeout(getWifiNetworks, 100);