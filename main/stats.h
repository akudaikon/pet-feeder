#ifndef STATS_H
#define STATS

#define STATS_NAMESPACE     "stats"

void stats_init();
esp_err_t stats_read();
void stats_update(bool forceUpdate);
void stats_setBowlAmount(uint8_t amount);
bool stats_isBowlAmountStable(uint16_t numStableReadings);
uint8_t stats_getBowlAmount();
void stats_setBinPercent(uint8_t percent);
bool stats_isBinPercentStable(uint16_t numStableReadings);
void stats_addFoodGiven(uint8_t initialAmount, uint8_t newAmount);
void stats_setFeedErrorFlag();
void stats_clearFeedErrorFlag();
void stats_reset();
char* stats_toJSON();
void stats_clear();

#endif