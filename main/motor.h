#ifndef MOTOR_H
#define MOTOR_H

void motor_init();
void motor_run();
void motor_stop();
bool motor_waitForRotationSensor(uint8_t transitions, uint16_t timeout_ms);

#endif