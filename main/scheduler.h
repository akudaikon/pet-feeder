#ifndef SCHEDULER_H
#define SCHEDULER_H

void scheduler_task(void *pvParameters);
void scheduler_stop();

#endif