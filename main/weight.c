#include "freertos/FreeRTOS.h"
#include "esp_log.h"

#include "NAU7802.h"
#include "settings.h"
#include "pins.h"
#include "weight.h"

#define AVG_ALPHA(x)        ((uint16_t)(x * 65535))
#define BOWL_AVG_ALPHA      AVG_ALPHA(0.1)
#define BIN_AVG_ALPHA       AVG_ALPHA(0.05)

static int32_t bowl_weight = -1;
static int32_t bin_weight = -1;
static int32_t water_weight = -1;

static int32_t bin_weight_old = -1;

static int32_t bowl_weight_avg = -1;
static int32_t bin_weight_avg = -1;
static int32_t water_weight_avg = -1;

static bool bowl_first_avg = true;
static bool bin_first_avg = true;
static bool water_first_avg = true;

void weight_init()
{
  NAU7802_init();
}

void weight_recalibrate()
{
  NAU7802_calibrate(NAU7802_CH_BOWL);
  NAU7802_calibrate(NAU7802_CH_BIN);
}

static int32_t calc_avg(int32_t new, int32_t average, uint16_t alpha)
{
  int64_t temp = (int64_t)new * (alpha) + (int64_t)average * (65536 - alpha);
  return (int32_t)((temp + 32768) / 65536);
}

bool weight_measureBowl()
{
  NAU7802_setChannel(NAU7802_CH_BOWL);
  NAU7802_readValue();  // flush past sample to ensure we get a current one
  bowl_weight = NAU7802_readValue();

  // If -1, invalid data from pending channel change
  if (bowl_weight == -1) return false;

  if (bowl_first_avg)
  {
    bowl_weight_avg = bowl_weight;
    bowl_first_avg = false;
  }
  else
  {
    bowl_weight_avg = calc_avg(bowl_weight, bowl_weight_avg, BOWL_AVG_ALPHA);
  }

  ESP_LOGI(__func__, "Bowl raw weight: %d, avg weight: %d", bowl_weight, bowl_weight_avg);
  return true;
}

int32_t weight_calibrateBowl()
{
  weight_resetAverages();
  for (uint8_t i = 0; i < 25; i++) weight_measureBowl(true);
  return bowl_weight_avg;
}

bool weight_measureBin()
{
  NAU7802_setChannel(NAU7802_CH_BIN);
  NAU7802_readValue();  // flush past sample to ensure we get a current one
  bin_weight = NAU7802_readValue();

  // If -1, invalid data from pending channel change
  if (bin_weight == -1) return false;

  // If bin weight changed too much (by > ~25%), more food was probably added. Reset average
  if ((bin_weight - bin_weight_old) > 200) bin_first_avg = true;
  bin_weight_old = bin_weight;

  if (bin_first_avg)
  {
    bin_weight_avg = bin_weight;
    bin_first_avg = false;
  }
  else
  {
    bin_weight_avg = calc_avg(bin_weight, bin_weight_avg, BIN_AVG_ALPHA);
  }

  ESP_LOGI(__func__, "Bin raw weight: %d, avg weight: %d", bin_weight, bin_weight_avg);
  return true;
}

int32_t weight_calibrateBin()
{
  weight_resetAverages();
  for (uint8_t i = 0; i < 25; i++) weight_measureBin(true);
  return bin_weight_avg;
}

void weight_measureWater()
{
  //water_weight = HX711_readValue();

  // if (water_first_avg)
  // {
  //   water_weight_avg = water_weight;
  //   water_first_avg = false;
  // }
  // else
  // {
  //   water_weight_avg = calc_avg(water_weight, water_weight_avg, NEW_AVG_WEIGHT);
  // }

  // ESP_LOGI(__func__, "Water raw weight: %d, avg weight: %d", water_weight, water_weight_avg);
}

int32_t weight_calibrateWater()
{
  weight_resetAverages();
  for (uint8_t i = 0; i < 50; i++) weight_measureWater();
  return water_weight_avg;
}

uint8_t weight_getBowlAmount()
{
  // Protect against div by 0
  if (settings.calibration_bowl_food == 0) return 0;
  
  uint8_t bowl_amount = 0;
  if (bowl_weight_avg <= settings.calibration_bowl_empty) bowl_amount = 0;
  else
  {
    bowl_amount = ((bowl_weight_avg - settings.calibration_bowl_empty) / settings.calibration_bowl_food);
  }
  bowl_amount -= (bowl_amount % 2);   // round down to nearest 0.25 cups
  return bowl_amount;
}

uint8_t weight_getBinPercentage()
{
  // Protect against div by 0
  if (settings.calibration_bin_full == 0) return 0;

  uint8_t percentage = 0;
  if (bin_weight_avg <= settings.calibration_bin_empty) percentage = 0;
  else
  {
    percentage = (((bin_weight_avg - settings.calibration_bin_empty) * 100) / 
                  (settings.calibration_bin_full - settings.calibration_bin_empty));
  }

  return (percentage > 100 ? 100 : percentage);
}

uint8_t weight_getWaterPercentage()
{
  // Protect against div by 0
  if ((settings.calibration_water_full - settings.calibration_water_empty) == 0) return 0;

  uint8_t percentage = ((water_weight_avg - settings.calibration_water_empty) /
                        (settings.calibration_water_full - settings.calibration_water_empty));

  return (percentage > 100 ? 100 : percentage);
}

void weight_resetAverages()
{
  bowl_first_avg = true;
  bin_first_avg = true;
  water_first_avg = true;
}
