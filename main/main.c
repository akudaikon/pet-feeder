// Built using ESP-IDF v4.0.1

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/gpio.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "mdns.h"

#include "pins.h"
#include "buttons.h"
#include "eventgroup_defs.h"
#include "settings.h"
#include "stats.h"
#include "wifi.h"
#include "httpserver.h"
#include "captdns.h"
#include "mqtt.h"
#include "util.h"
#include "IS31FL3193.h"
#include "scheduler.h"
#include "weight.h"
#include "motor.h"

#define BOWL_REFILL_DELAY   150  // ~5 minutes
#define MQTT_UPDATE_RATE    120  // 2 minutes

#define MQTT_UPDATE_NOW     ((__UINT16_MAX__) - 1)

struct settingsData_t settings;
EventGroupHandle_t xEventGroup = NULL;

enum
{
  MEASURE_STATE_BIN,
  MEASURE_STATE_BOWL,
  MEASURE_STATE_RECALIBRATE
};

static void setBinAmountLEDColor(uint8_t binPercent)
{
  if (binPercent > SETTINGS_BIN_WARNING_PERCENT)
  {
    // Set LED to solid green
    led_disableBreathing();
    led_setColor(0x22, 0xFF, 0x00);
  }
  else if (binPercent > SETTINGS_BIN_LOW_PERCENT)
  {
    // Set LED to solid yellow
    led_disableBreathing();
    led_setColor(0xFF, 0xD5, 0x00);
  }
  else if (binPercent > SETTINGS_BIN_EMPTY_PERCENT)
  {
    // Set LED to solid red
    led_disableBreathing();
    led_setColor(0xFF, 0x00, 0x00);
  }
  else
  {
    // Set LED to breathe red
    led_enableBreathing(LED_OFF_TIME_520MS, LED_ON_TIME_520MS, LED_FADE_TIME_520MS);
    led_setColor(0xFF, 0x00, 0x00);
  }
}

static void handleCalibration()
{
  if (xEventGroupGetBits(xEventGroup) & CALIB_BIN_EMPTY)
  {
    settings.calibrated &= ~(SETTINGS_CALIBRATION_BIN_EMPTY);
    if (buttons_isBinPresent() == true)
    {
      settings.calibration_bin_empty = weight_calibrateBin() + 10;
      settings.calibrated |= SETTINGS_CALIBRATION_BIN_EMPTY;
      settings_write();
    }
    xEventGroupClearBits(xEventGroup, CALIB_BIN_EMPTY);
  }
  else if (xEventGroupGetBits(xEventGroup) & CALIB_BIN_FULL)
  {
    settings.calibrated &= ~(SETTINGS_CALIBRATION_BIN_FULL);
    if (buttons_isBinPresent() == true)
    {
      settings.calibration_bin_full = weight_calibrateBin() - 10;
      settings.calibrated |= SETTINGS_CALIBRATION_BIN_FULL;
      settings_write();
    }
    xEventGroupClearBits(xEventGroup, CALIB_BIN_FULL);
  }
  else if (xEventGroupGetBits(xEventGroup) & CALIB_BOWL_FOOD)
  {
    settings.calibrated &= ~(SETTINGS_CALIBRATION_BOWL_FOOD);
    if (buttons_isBinPresent() == true)
    {
      int32_t samples[4] = { 0 };

      // Measure empty bowl
      settings.calibration_bowl_empty = weight_calibrateBowl();

      // Take 4 samples of food in bowl
      for (uint8_t i = 0; i < 4; i++)
      {
        motor_run();
        motor_waitForRotationSensor(4, 5000);
        motor_stop();
        samples[i] = weight_calibrateBowl() - settings.calibration_bowl_empty;
      }

      // Calculate differences between samples
      for (uint8_t i = 1; i < 4; i++) samples[4 - i] = samples[4 - i] - samples[4 - i - 1];

      // Find lowest sample so we can throw it out
      int32_t lowest = __INT32_MAX__;
      for (uint8_t i = 0; i < 4; i++) if (samples[i] < lowest) lowest = samples[i];

      // Find sample average (minus the lowest)
      settings.calibration_bowl_food = (samples[0] + samples[1] + samples[2] + samples[3] - lowest) / 3;

      settings.calibrated |= SETTINGS_CALIBRATION_BOWL_FOOD;
      settings_write();
    }
    xEventGroupClearBits(xEventGroup, CALIB_BOWL_FOOD);
  }
  else if (xEventGroupGetBits(xEventGroup) & CALIB_WATER_EMPTY)
  {
    settings.calibrated &= ~(SETTINGS_CALIBRATION_WATER_EMPTY);
    settings.calibration_water_empty = weight_calibrateWater();
    settings.calibrated |= SETTINGS_CALIBRATION_WATER_EMPTY;
    settings_write();
    xEventGroupClearBits(xEventGroup, CALIB_WATER_EMPTY);
  }
  else if (xEventGroupGetBits(xEventGroup) & CALIB_WATER_FULL)
  {
    settings.calibrated &= ~(SETTINGS_CALIBRATION_WATER_FULL);
    settings.calibration_water_full = weight_calibrateWater();
    settings.calibrated |= SETTINGS_CALIBRATION_WATER_FULL;
    settings_write();
    xEventGroupClearBits(xEventGroup, CALIB_WATER_FULL);
  }
}

void app_main()
{
  xEventGroup = xEventGroupCreate();
  settings.mqttConnected = false;
  settings.firmwareUpdating = false;

  uint8_t measureState = MEASURE_STATE_BIN;

  uint8_t loopTime = 0;
  uint16_t mqttUpdateTime = (MQTT_UPDATE_RATE - 60);

  bool flag_binPresenceState = true;
  bool flag_bowlNeedRefill = false;
  bool flag_ledIsDimmed = false;

  nvs_init();
  settings_read();
  spiffs_mount();

  i2c_init();
  weight_init();
  led_init();
  motor_init();
  stats_init();

  // Start button task
  xTaskCreatePinnedToCore(&buttons_task, "button", 2048, NULL, 3, NULL, 1);
  xEventGroupWaitBits(xEventGroup, BUTTON_TASK_STARTED, pdFALSE, pdFALSE, portMAX_DELAY);

  // Factory reset if button held at startup
  if (buttons_held())
  {
    ESP_LOGI(__func__, "Performing factory reset...");

    // Set LED to breathe red quickly
    led_enableBreathing(LED_OFF_TIME_130MS, LED_ON_TIME_130MS, LED_FADE_TIME_130MS);
    led_setColor(0xFF, 0x00, 0x00);
    vTaskDelay(3000 / portTICK_PERIOD_MS);

    // Clear settings and restart
    settings_clear();
    led_disableLEDs();
    esp_restart();
  }

  // If we haven't been configured, start up AP and webserver
  while (!settings.configured)
  {
    wifi_init();
    wifi_apStart();
    captDNS_start();
    httpServer_start();

    // Wait for AP and webserver to come up
    xEventGroupWaitBits(xEventGroup, HTTP_SERVER_STARTED, pdFALSE, pdFALSE, portMAX_DELAY);

    // Set LED to breathe yellow
    led_enableBreathing(LED_OFF_TIME_520MS, LED_ON_TIME_260MS, LED_FADE_TIME_520MS);
    led_setColor(0xFF, 0xD5, 0x00);

    // Wait for config to be finished
    while (!(xEventGroupGetBits(xEventGroup) & CONFIG_DONE)) vTaskDelay(100 / portTICK_PERIOD_MS);

    // We're done, stop AP and webserver
    captDNS_stop();
    httpServer_stop();
    wifi_disconnect();

    // set LED to breathe blue
    led_enableBreathing(LED_OFF_TIME_520MS, LED_ON_TIME_260MS, LED_FADE_TIME_520MS);
    led_setColor(0x3C, 0xFF, 0xFF);

    // Try new wifi settings to make sure they're valid
    wifi_connect(settings.wifiSSID, settings.wifiPassword);

    if (wifi_waitUntilConnected() == ESP_OK)
    {
      // Set LED to solid green
      led_disableBreathing();
      led_setColor(0x22, 0xFF, 0x00);
      vTaskDelay(3000 / portTICK_PERIOD_MS);

      // Save settings and restart
      wifi_disconnect();
      settings.configured = 1;
      settings_write();
      led_disableLEDs();
      esp_restart();
    }
  }

  // Set LED to breathe blue
  led_enableBreathing(LED_OFF_TIME_520MS, LED_ON_TIME_260MS, LED_FADE_TIME_520MS);
  led_setColor(0x3C, 0xFF, 0xFF);

  // Connect to Wifi
  wifi_init();
  wifi_connect(settings.wifiSSID, settings.wifiPassword);

  // Wait for Wifi to connect
  xEventGroupWaitBits(xEventGroup, WIFI_CONNECTED, pdFALSE, pdFALSE, portMAX_DELAY);

  // Start mDNS server
  if (mdns_init() == ESP_OK)
  {
    mdns_hostname_set("petfeeder");
    mdns_instance_name_set("Pet Feeder");
    ESP_LOGI(__func__, "MDNS server started");
  }

  // Start HTTP server
  httpServer_start();

  // Make sure calibration has been done
  if (settings.calibrated != SETTINGS_CALIBRATION_DONE)
  {
    // Set LED to breathe purple
    led_enableBreathing(LED_OFF_TIME_520MS, LED_ON_TIME_260MS, LED_FADE_TIME_520MS);
    led_setColor(0xBC, 0x2F, 0xFF);

    ESP_LOGI(__func__, "Waiting for calibration to be completed...");

    // Wait for calibration to be finished and handle calibration events
    while (settings.calibrated != SETTINGS_CALIBRATION_DONE)
    {
      handleCalibration();
      vTaskDelay(100 / portTICK_PERIOD_MS);
    }
  }

  // Start MQTT client (if enabled)
  if (settings.mqttEnable) mqtt_start(settings.mqttURI);

  // Sync time
  syncTime();

  // Start scheduler task
  xTaskCreatePinnedToCore(&scheduler_task, "scheduler", 2048, NULL, 2, NULL, 1);

  // Read statistics and read initial weights
  stats_read();
  weight_measureBowl();
  stats_setBowlAmount(weight_getBowlAmount());
  weight_measureBin();
  stats_setBinPercent(weight_getBinPercentage());

  // Set LED to solid green
  led_disableBreathing();
  led_setColor(0x22, 0xFF, 0x00);
  vTaskDelay(1000 / portTICK_PERIOD_MS);

  while(1)
  {
    // Skip main loop while firmware update in progress
    if (settings.firmwareUpdating)
    {
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      continue;
    }

    // Handle any recalibration requests
    handleCalibration();

    // Reset averages when button pressed. Ex: when bin refilled
    if (xEventGroupGetBits(xEventGroup) & BUTTON_PRESS) weight_resetAverages();

    // Feed if scheduler event or long button press
    if ((xEventGroupGetBits(xEventGroup) & SCHEDULER_EVENT) || (xEventGroupGetBits(xEventGroup) & BUTTON_LONG_PRESS))
    {
      // Make sure bin is present and there's food in it
      if (buttons_isBinPresent() && weight_getBinPercentage() > 0)
      {
        // Get current weight of bowl
        weight_measureBowl();
        uint8_t initialBowlAmount = weight_getBowlAmount();
        uint8_t currentBowlAmount = initialBowlAmount;

        // If we need to add more...
        if (currentBowlAmount < settings.feedAmount)
        {
          uint8_t lastBowlAmount = 0;
          uint8_t noChangeCount = 0;

          flag_bowlNeedRefill = false;

          ESP_LOGI(__func__, "Feeding %d servings...", (settings.feedAmount - currentBowlAmount));

          // Set LED to breathe white
          led_enableBreathing(LED_OFF_TIME_520MS, LED_ON_TIME_260MS, LED_FADE_TIME_520MS);
          led_setColor(0xFF, 0xFF, 0xFF);

          // Add as much as needed
          motor_run();
          while (currentBowlAmount < settings.feedAmount)
          {
            if (motor_waitForRotationSensor(1, 5000) == false)
            {
              // Timed out waiting for the rotation sensor, something went wrong!
              motor_stop();
              ESP_LOGI(__func__, "Wait for rotation sensor timed out!");

              // Set LED to breathe red quickly
              led_enableBreathing(LED_OFF_TIME_0MS, LED_ON_TIME_130MS, LED_FADE_TIME_130MS);
              led_setColor(0xFF, 0x00, 0x00);
              vTaskDelay(3000 / portTICK_PERIOD_MS);

              stats_setFeedErrorFlag();
              break;
            }

            // Remeasure current amount in bowl
            weight_resetAverages();
            weight_measureBowl();
            currentBowlAmount = weight_getBowlAmount();

            // If bowl amount hasn't changed in a while, something went wrong!
            if (currentBowlAmount - lastBowlAmount == 0) noChangeCount++;
            else noChangeCount = 0;

            if (noChangeCount > 50)
            {
              motor_stop();
              ESP_LOGI(__func__, "Bowl amount hasn't changed in too long! No more food?");

              // Set LED to breathe red quickly
              led_enableBreathing(LED_OFF_TIME_0MS, LED_ON_TIME_130MS, LED_FADE_TIME_130MS);
              led_setColor(0xFF, 0x00, 0x00);
              vTaskDelay(3000 / portTICK_PERIOD_MS);

              stats_setFeedErrorFlag();
              break;
            }

            lastBowlAmount = currentBowlAmount;
          }
          motor_stop();

          // Update statistics
          stats_setBowlAmount(currentBowlAmount);
          stats_addFoodGiven(initialBowlAmount, currentBowlAmount);
          stats_update(true);  // force update

          mqttUpdateTime = MQTT_UPDATE_NOW;  // Force MQTT update
        }
        else
        {
          ESP_LOGI(__func__, "Bowl already full!");
          if (settings.enableMissedFeed && xEventGroupGetBits(xEventGroup) & SCHEDULER_EVENT) flag_bowlNeedRefill = true;
        }
      }
      // Bin isn't present or there's no food
      else
      {
        // Set LED to breathe red quickly
        led_enableBreathing(LED_OFF_TIME_0MS, LED_ON_TIME_130MS, LED_FADE_TIME_130MS);
        led_setColor(0xFF, 0x00, 0x00);
        vTaskDelay(3000 / portTICK_PERIOD_MS);
      }

      // Clear flags that got us here
      xEventGroupClearBits(xEventGroup, (SCHEDULER_EVENT | BUTTON_LONG_PRESS));
    }

    // Reset averages and remeasure weights if bin presence state changed
    if (buttons_isBinPresent() != flag_binPresenceState)
    {
      weight_resetAverages();
      flag_binPresenceState = buttons_isBinPresent();
      if (flag_binPresenceState == false) setBinAmountLEDColor(0);
      ESP_LOGI(__func__, "Bin presence state changed to %s", (flag_binPresenceState ? "true" : "false"));
    }

    // One second loop
    if (++loopTime > 10)
    {
      loopTime = 0;

      // State machine to toggle between reading bin and bowl weights and recalibration
      switch (measureState)
      {
        case MEASURE_STATE_BIN:
        {
          if (weight_measureBin())
          {
            uint8_t binPercent = weight_getBinPercentage();
            stats_setBinPercent(binPercent);
            setBinAmountLEDColor(binPercent);
            ESP_LOGI(__func__, "Bin full percentage: %d", binPercent);
            measureState = MEASURE_STATE_BOWL;
          }
          break;
        }

        case MEASURE_STATE_BOWL:
        {
          if (weight_measureBowl())
          {
            uint8_t bowlAmount = weight_getBowlAmount();
            stats_setBowlAmount(bowlAmount);
            ESP_LOGI(__func__, "Bowl amount: %d", bowlAmount);
            measureState = MEASURE_STATE_BIN;
          }
          break;
        }

        case MEASURE_STATE_RECALIBRATE:
          ESP_LOGI(__func__, "Recalibrating weight...");
          weight_recalibrate();
          measureState = MEASURE_STATE_BIN;
        break;
      }

      // Update statistics (don't force update)
      stats_update(false);

      // If bowl was already full last time we tried to add more, or if we're in "always full" mode...
      if (flag_bowlNeedRefill || (settings.feedMode == SETTINGS_FEEDMODE_ALWAYSFULL))
      {
        // If it's not full now and hasn't been for a while, try to add more
        if ((weight_getBowlAmount() < settings.feedAmount) && stats_isBowlAmountStable(BOWL_REFILL_DELAY)) 
        { 
          xEventGroupSetBits(xEventGroup, SCHEDULER_EVENT);
        }
      }

      // Send an MQTT stats update every 5 mins
      if (settings.mqttEnable && ++mqttUpdateTime > MQTT_UPDATE_RATE)
      {
        mqtt_sendUpdate();
        stats_clearFeedErrorFlag();
        mqttUpdateTime = 0;
      }
    }

    // Reset statistics on end of day
    if (xEventGroupGetBits(xEventGroup) & END_OF_DAY_BEFORE)
    {
      // Force update any pending statistics updates before end of day
      stats_update(true);
      mqttUpdateTime = MQTT_UPDATE_NOW;
      weight_recalibrate();
      xEventGroupClearBits(xEventGroup, END_OF_DAY_BEFORE);
    }
    if (xEventGroupGetBits(xEventGroup) & END_OF_DAY_AFTER)
    {
      // Reset statistics and force update at end of day
      stats_reset();
      weight_measureBowl();
      stats_setBowlAmount(weight_getBowlAmount());
      stats_update(true);  // force update
      mqttUpdateTime = MQTT_UPDATE_NOW;  // Force MQTT update
      xEventGroupClearBits(xEventGroup, END_OF_DAY_AFTER);
    }

    // Dim LED during certain time frame
    if ((xEventGroupGetBits(xEventGroup) & LED_DIM) && !flag_ledIsDimmed)
    {
      led_setCurrent(LED_CURRENT_8MA);
      flag_ledIsDimmed = true;
    }
    else if (!(xEventGroupGetBits(xEventGroup) & LED_DIM) && flag_ledIsDimmed)
    {
      led_setCurrent(LED_CURRENT_30MA);
      flag_ledIsDimmed = false;
    }

    vTaskDelay(100 / portTICK_PERIOD_MS);
  }

  return;
}
