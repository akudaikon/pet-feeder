#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := petfeeder

include $(IDF_PATH)/make/project.mk

# This part gets the information for the spiffs partition
ifneq ("$(wildcard $(PROJECT_PATH)/build/partitions.bin)","")
comma := ,

SPIFFS_PARTITION_INFO := $(shell $(IDF_PATH)/components/partition_table/gen_esp32part.py --quiet $(PROJECT_PATH)/build/partitions.bin | grep "spiffs")

SPIFFS_BASE_ADDR  := $(word 4, $(subst $(comma), , $(SPIFFS_PARTITION_INFO)))
SPIFFS_SIZE_INFO  := $(word 5, $(subst $(comma), , $(SPIFFS_PARTITION_INFO)))
SPIFFS_SIZE_UNITS := $(word 1, $(subst M, M, $(subst K, K, $(word 5, $(subst $(comma), , $(SPIFFS_PARTITION_INFO))))))
SPIFFS_SIZE_UNIT  := $(word 2, $(subst M, M, $(subst K, K, $(word 5, $(subst $(comma), , $(SPIFFS_PARTITION_INFO))))))

SPIFFS_SIZE_FACTOR := 1
ifeq ($(SPIFFS_SIZE_UNIT), K)
SPIFFS_SIZE_FACTOR := 1024
endif

ifeq ($(SPIFFS_SIZE_UNIT), M)
SPIFFS_SIZE_FACTOR := 1048576
endif

ifeq ("foo$(SPIFFS_SIZE_UNIT)", "foo")
SPIFFS_SIZE_UNITS := 512
SPIFFS_SIZE_FACTOR := 1024
endif

SPIFFS_SIZE := $(shell expr $(SPIFFS_SIZE_UNITS) \* $(SPIFFS_SIZE_FACTOR))
endif

spiffs:
	@$(IDF_PATH)/components/partition_table/gen_esp32part.py --quiet $(PROJECT_PATH)/partitions.csv $(PROJECT_PATH)/build/partitions.bin
	@echo "Building SPIFFS image..."
	@echo "Base address $(SPIFFS_BASE_ADDR), size $(SPIFFS_SIZE) bytes"
	$(IDF_PATH)/mkspiffs/mkspiffs -c main/spiffs -b 4096 -p 256 -s $(SPIFFS_SIZE) $(PROJECT_PATH)/build/spiffs.bin
	$(ESPTOOLPY_WRITE_FLASH) -z 0x310000 $(PROJECT_PATH)/build/spiffs.bin

reset_factory:
	$(ESPTOOLPY_SERIAL) erase_region 0xd000 0x2000